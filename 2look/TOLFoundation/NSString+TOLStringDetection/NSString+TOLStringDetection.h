//
//  NSString+TOLStringDetection.h
//  2look
//
//  Created by Marcio Klepacz on 6/27/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TOLStringDetection)

- (BOOL) isSectionWithLookCategory;

- (BOOL) isSectionWithFeed;

@end
