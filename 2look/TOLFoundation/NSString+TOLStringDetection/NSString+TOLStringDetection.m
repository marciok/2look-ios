//
//  NSString+TOLStringDetection.m
//  2look
//
//  Created by Marcio Klepacz on 6/27/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "NSString+TOLStringDetection.h"

@implementation NSString (TOLStringDetection)

- (BOOL)isSectionWithLookCategory
{
    return [self isEqualToString:@"garments"] || [self isEqualToString:@"occasions"] || [self isEqualToString:@"suppliers"];
}

- (BOOL)isSectionWithFeed
{
    return [self isEqualToString:@"feed"] || [self isEqualToString:@"favorites"];
}

@end
