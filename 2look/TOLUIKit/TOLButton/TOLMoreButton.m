//
//  TOLMoreButton.m
//  2look
//
//  Created by Marcio Klepacz on 11/17/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLMoreButton.h"

@interface TOLMoreButton ()

@property (nonatomic, strong) CAShapeLayer *buttonShapeLayer;
@property (nonatomic, strong) CAShapeLayer *firstDotLayer;
@property (nonatomic, strong) CAShapeLayer *secondDotLayer;
@property (nonatomic, strong) CAShapeLayer *middleDotLayer;

@property (nonatomic, assign) CGPathRef originalFirstRect;
@property (nonatomic, assign) CGPathRef originalSecondRect;

@end

@implementation TOLMoreButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (!self) {
        return nil;
    }
   
    [self setupLayers];
    
    [self addTarget:self action:@selector(handleButtonTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(handleButtonTouchDown) forControlEvents:UIControlEventTouchDown];
    
    return self;
}

- (void) setupLayers
{
    self.buttonShapeLayer = [CAShapeLayer layer];
    self.buttonShapeLayer.fillColor = [UIColor clearColor].CGColor;
    self.buttonShapeLayer.strokeColor = [TOLColor darkGrayColor].CGColor;
    self.buttonShapeLayer.lineWidth = 0.5;
    [self.layer addSublayer:self.buttonShapeLayer];
    
    self.firstDotLayer = [CAShapeLayer layer];
    self.firstDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    [self.layer addSublayer:self.firstDotLayer];
    
    self.secondDotLayer = [CAShapeLayer layer];
    self.secondDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    [self.layer addSublayer:self.secondDotLayer];
    
    self.middleDotLayer = [CAShapeLayer layer];
    self.middleDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    [self.layer addSublayer:self.middleDotLayer];
    
    // Button stroke Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(self.bounds) + 0.25, CGRectGetMinY(self.bounds) - 0.25, 26, 26)];
    self.buttonShapeLayer.path = ovalPath.CGPath;
    
    
    // First Oval Drawing
    UIBezierPath* firstOvalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(self.bounds) + 4, CGRectGetMinY(self.bounds) + 11, 4, 4)];
    self.firstDotLayer.path = firstOvalPath.CGPath;
    self.originalFirstRect = CGPathCreateCopy(firstOvalPath.CGPath);
    
    // Third Oval Drawing
    UIBezierPath* thirdOvalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(self.bounds) + 19, CGRectGetMinY(self.bounds) + 11, 4, 4)];
    self.secondDotLayer.path = thirdOvalPath.CGPath;
    self.originalSecondRect = CGPathCreateCopy(thirdOvalPath.CGPath);
    
    
    // Second Oval Drawing
    UIBezierPath* secondOvalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(self.bounds) + 12, CGRectGetMinY(self.bounds) + 11, 4, 4)];
    
    self.middleDotLayer.path = secondOvalPath.CGPath;
    
}

#pragma mark - UIControlEvents

- (void)handleButtonTouchDown
{
    self.buttonShapeLayer.strokeColor = [UIColor colorWithRed:0.35 green:0.78 blue:0.98 alpha:1].CGColor;
    
    self.firstDotLayer.fillColor = [UIColor colorWithRed:0.35 green:0.78 blue:0.98 alpha:1].CGColor;
    self.secondDotLayer.fillColor = [UIColor colorWithRed:0.35 green:0.78 blue:0.98 alpha:1].CGColor;
    self.middleDotLayer.fillColor = [UIColor colorWithRed:0.35 green:0.78 blue:0.98 alpha:1].CGColor;
    
    self.firstDotLayer.path = self.middleDotLayer.path;
    self.secondDotLayer.path = self.middleDotLayer.path;
}

- (void)handleButtonTouchUpInside
{
    self.buttonShapeLayer.strokeColor = [TOLColor darkGrayColor].CGColor;
    self.firstDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    self.secondDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    self.middleDotLayer.fillColor = [TOLColor darkGrayColor].CGColor;
    
    self.firstDotLayer.path = self.originalFirstRect;
    self.secondDotLayer.path = self.originalSecondRect;
    
    [self.delegate touchUpInside];
}

@end
