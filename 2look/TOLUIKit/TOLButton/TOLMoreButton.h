//
//  TOLMoreButton.h
//  2look
//
//  Created by Marcio Klepacz on 11/17/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TOLMoreButtonDelegate <NSObject>

@required

-(void)touchUpInside;

@end

@interface TOLMoreButton : UIButton

@property (nonatomic, weak) id<TOLMoreButtonDelegate> delegate;

@end
