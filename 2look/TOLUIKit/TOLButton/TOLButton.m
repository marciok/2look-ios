//
//  TOLConfigurationButton.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLButton.h"

@implementation TOLButton

+ (UIButton *)buttonWithConfigurationType
{
    //Creating Configuration Button;
    UIButton *configurationButton = [super buttonWithType:UIButtonTypeCustom];
    
    configurationButton.titleLabel.font = [UIFont fontForSubtitle];
    [configurationButton setTitleColor:[TOLColor darkGrayColor] forState:UIControlStateNormal];
    [configurationButton setTitle:NSLocalizedString(@"Configurations", @"Configurations") forState:UIControlStateNormal];
    
    return configurationButton;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

@end
