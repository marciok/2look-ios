//
//  TOLMenuTableViewCell.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLMenuTableViewCell.h"

@interface TOLMenuTableViewCell()

@end

@implementation TOLMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    
    //TODO: Comeback to improve this code;
    self.frame = CGRectMake(0, 0, self.frame.size.width, 50);
    self.menuCellBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, 16, 45, self.frame.size.height)];
    [self.contentView addSubview:self.menuCellBackgroundView];
    self.menuCellBackgroundView.frame = CGRectMake(0, 0, 150, self.frame.size.height);
    
    self.menuCellTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, self.frame.size.height)];
    self.menuCellTitle.font = [UIFont fontForTitle];
    self.menuCellTitle.textColor = [TOLColor basicWhiteColor];
    
    [self.contentView addSubview:self.menuCellTitle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.menuCellTitle.textColor = [TOLColor basicWhiteColor];
    }
    // Configure the view for the selected state
}

@end
