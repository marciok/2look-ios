//
//  TOLMenuTableViewCell.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MenuItemType) {
    FeedMenuItem,
    WhoMenuItem,
    WhenMenuItem,
    WhatMenuItem,
    FavoritesMenuItem
};

@interface TOLMenuTableViewCell : UITableViewCell

@property (nonatomic, assign) TOLLookCategoryType lookCategoryType;
@property (nonatomic, strong) UIView *menuCellBackgroundView;
@property (nonatomic, strong) UILabel *menuCellTitle;

@end
