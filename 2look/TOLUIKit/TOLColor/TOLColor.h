//
//  TOLColor.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLColor : UIColor

+ (UIColor *)lightBlueColorHalfAlpha;

+ (UIColor *)lightGreenColorHalfAlpha;

+ (UIColor *)lightRedColorHalfAlpha;

+ (UIColor *)lightOrangeColorHalfAlpha;

+ (UIColor *)lightYellowColorHalfAlpha;

+ (UIColor *)lightGrayColorHalfAlpha;

+ (UIColor *)lightBlueColor;

+ (UIColor *)lightGreenColor;

+ (UIColor *)lightRedColor;

+ (UIColor *)lightOrangeColor;

+ (UIColor *)lightYellowColor;

+ (UIColor *)lightGrayColor;

+ (UIColor *)ultraLightGrayColor;

+ (UIColor *)basicWhiteColor;

+ (UIColor *)darkGrayColor;

+ (UIColor *)darkGreenColor;

+ (UIColor *)darkBlueColor;

+ (UIColor *)darkRedColor;

+ (UIColor *)darkOrangeColor;

+ (UIColor *)darkYellowColor;

+ (UIColor *)addToFavoritesMarkerColor;

+ (UIColor *)addToFavoritesStarColor;

@end
