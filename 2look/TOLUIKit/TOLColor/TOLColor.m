//
//  TOLColor.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLColor.h"

@implementation TOLColor

#pragma mark Colors

+ (UIColor *)lightBlueColorHalfAlpha
{
    return [super colorWithRed:15/255.0 green:153/255.0 blue:153/255.0 alpha:0.3];
}

+ (UIColor *)lightGreenColorHalfAlpha
{
    return [super colorWithRed:0/255.0 green:212/255.0 blue:135/255.0 alpha:0.3];
}

+ (UIColor *)lightRedColorHalfAlpha
{
    return [super colorWithRed:211/255.0 green:67/255.0 blue:111/255.0 alpha:0.3];
}

+ (UIColor *)lightOrangeColorHalfAlpha
{
    return [super colorWithRed:220/255.0 green:164/255.0 blue:66/255.0 alpha:0.3];
}

+ (UIColor *)lightYellowColorHalfAlpha
{
    return [super colorWithRed:226/255.0 green:221/255.0 blue:61/255.0 alpha:0.3];
}

+ (UIColor *)lightGrayColorHalfAlpha
{
    return [super colorWithRed:209/255.0 green:209/255.0 blue:210/255.0 alpha:0.3];
}


#pragma mark -

+ (UIColor *)lightBlueColor
{
    return [super colorWithRed:15/255.0 green:153/255.0 blue:153/255.0 alpha:1];
}

+ (UIColor *)lightGreenColor
{
    return [super colorWithRed:0/255.0 green:212/255.0 blue:135/255.0 alpha:1];
}

+ (UIColor *)lightRedColor
{
    return [super colorWithRed:211/255.0 green:67/255.0 blue:111/255.0 alpha:1];
}

+ (UIColor *)lightOrangeColor
{
    return [super colorWithRed:220/255.0 green:164/255.0 blue:66/255.0 alpha:1];
}

+ (UIColor *)lightYellowColor
{
    return [super colorWithRed:226/255.0 green:221/255.0 blue:61/255.0 alpha:1];
}

+ (UIColor *)lightGrayColor
{
    return [super colorWithRed:209/255.0 green:209/255.0 blue:210/255.0 alpha:1];
}

+ (UIColor *)ultraLightGrayColor
{
    return [super colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];
}

+ (UIColor *)basicWhiteColor
{
    return [super whiteColor];
}

+ (UIColor *)darkGrayColor
{
    return [super colorWithRed:127/255.0 green:127/255.0 blue:127/255.0 alpha:1];
}

+ (UIColor *)darkGreenColor
{
    return [super colorWithRed:0/255.0 green:169/255.0 blue:108/255.0 alpha:1];
}

+ (UIColor *)darkBlueColor
{
    return [super colorWithRed:0/255.0 green:128/255.0 blue:128/255.0 alpha:1];
}

+ (UIColor *)darkRedColor
{
    return [super colorWithRed:160/255.0 green:25/255.0 blue:66/255.0 alpha:1];
}

+ (UIColor *)darkOrangeColor
{
    return [super colorWithRed:153/255.0 green:102/255.0 blue:13/255.0 alpha:1];
}

+ (UIColor *)darkYellowColor
{
    return [super colorWithRed:158/255.0 green:154/255.0 blue:20/255.0 alpha:1];
}

+ (UIColor *)addToFavoritesMarkerColor
{
    return [super colorWithRed:1 green:0.82 blue:0 alpha:1];
}

+ (UIColor *)addToFavoritesStarColor
{
    return [super colorWithRed:0.8 green:0.66 blue:0 alpha:1];
}

@end
