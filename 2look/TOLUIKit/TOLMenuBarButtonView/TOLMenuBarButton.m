//
//  TOLMenuBarButtonView.m
//  2look
//
//  Created by Marcio Klepacz on 25/12/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLMenuBarButton.h"

static CGFloat const kTOLBarHeight = 2;

@implementation TOLMenuBarButton

+ (instancetype)button
{
    TOLMenuBarButton *button = [TOLMenuBarButton buttonWithType:UIButtonTypeCustom];
    [button sizeToFit];
    
    if (!button) {
        return nil;
    }
    
    [button setupLayers];
    
    return button;
}

- (void)setupLayers
{
    CAShapeLayer *firstBar = [CAShapeLayer layer];
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(12, 8, 16, kTOLBarHeight)];
    firstBar.path = rectanglePath.CGPath;
    
    //// Rectangle 2 Drawing
    CAShapeLayer *secondBar = [CAShapeLayer layer];
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(10, 14, 20, kTOLBarHeight)];
    secondBar.path = rectangle2Path.CGPath;
    
    //// Rectangle 3 Drawing
    CAShapeLayer *thirdBar = [CAShapeLayer layer];
    UIBezierPath* rectangle3Path = [UIBezierPath bezierPathWithRect: CGRectMake(10, 20, 20, kTOLBarHeight)];
    thirdBar.path = rectangle3Path.CGPath;
    
    firstBar.fillColor = [UIColor whiteColor].CGColor;
    secondBar.fillColor = [UIColor whiteColor].CGColor;
    thirdBar.fillColor = [UIColor whiteColor].CGColor;
    
    [self.layer addSublayer:firstBar];
    [self.layer addSublayer:secondBar];
    [self.layer addSublayer:thirdBar];
}


@end
