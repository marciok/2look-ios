//
//  TOLMenuBarButtonView.h
//  2look
//
//  Created by Marcio Klepacz on 25/12/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLMenuBarButton : UIButton

+ (instancetype)button;

@end
