//
//  TOLAddToFavoritesView.h
//  2look
//
//  Created by Marcio Klepacz on 11/6/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <pop/POP.h>

@protocol TOLPullToFavoriteDelegate <NSObject>

@required

- (void)addToFavorites;

- (void)removeFromFavorites;

@end

@interface TOLAddToFavoritesView : UIView <POPAnimationDelegate>

@property (nonatomic, weak) id<TOLPullToFavoriteDelegate> pullToFavoriteDelegate;
@property (nonatomic, assign, getter=isFavorited) BOOL favorited;

@end

