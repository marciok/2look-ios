//
//  TOLAddToFavoritesView.m
//  2look
//
//  Created by Marcio Klepacz on 11/6/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLAddToFavoritesView.h"
#import "User.h"

static CGFloat const kTOLMarkerMaximumHeight = 120;
static CGFloat const kTOLMarkerExpandedHeight = 80;
static CGFloat const kTOLMinimumDistanceToUnstar = 30;
static NSString *const kTOLSlidingToOpenAnimationIdentifier = @"slidingToOpen";
static NSString *const kTOLSlidingToCloseAnimationIdentifier = @"slidingToClose";

@interface TOLAddToFavoritesView ()

@property (nonatomic, assign) CGFloat initialHeight;
@property (nonatomic, strong) CAShapeLayer *markerFooterLayer;
@property (nonatomic, strong) POPSpringAnimation *expandMarkerAnimation;
@property (nonatomic, strong) POPSpringAnimation *appearingStarAnimation;
@property (nonatomic, strong) CAShapeLayer *starShapeLayer;
@property (nonatomic, assign) BOOL expandAnimationHasStarted;

@end

@implementation TOLAddToFavoritesView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    [self setupView];
    
    return self;
}

- (void)setupView
{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleOpenPanGesture:)];
    [self addGestureRecognizer:recognizer];

    self.initialHeight = CGRectGetHeight(self.bounds);
    
    self.backgroundColor = [TOLColor addToFavoritesMarkerColor];
    self.userInteractionEnabled = YES;
    
    self.markerFooterLayer = [CAShapeLayer layer];
    self.markerFooterLayer.frame = self.bounds;
    self.layer.mask = self.markerFooterLayer;

    self.starShapeLayer.opacity = 0;
    [self.layer addSublayer:self.starShapeLayer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //Setup marker footer
    UIBezierPath *markerFooterPath = [UIBezierPath bezierPath];
    
    [markerFooterPath moveToPoint:CGPointMake(CGRectGetMinX(self.bounds), CGRectGetMinY(self.bounds))];
    [markerFooterPath addLineToPoint:CGPointMake(CGRectGetMaxX(self.bounds), CGRectGetMinY(self.bounds))];
    [markerFooterPath addLineToPoint:CGPointMake(CGRectGetMaxX(self.bounds), CGRectGetMaxY(self.bounds))];
    [markerFooterPath addLineToPoint:CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetMaxY(self.bounds) - 10)];
    [markerFooterPath addLineToPoint:CGPointMake(CGRectGetMinX(self.bounds), CGRectGetMaxY(self.bounds))];
    [markerFooterPath closePath];
    
    self.markerFooterLayer.path = markerFooterPath.CGPath;
}

#pragma mark - Attrs

-(POPSpringAnimation *)appearingStarAnimation
{
    if (_appearingStarAnimation) {
        return  _appearingStarAnimation;
    }
    
    _appearingStarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    
    return _appearingStarAnimation;
}

- (POPSpringAnimation *)expandMarkerAnimation
{
    if (_expandMarkerAnimation) {
        return _expandMarkerAnimation;
    }
    
    _expandMarkerAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    _expandMarkerAnimation.springSpeed = 20;
    _expandMarkerAnimation.springBounciness = 10;
    _expandMarkerAnimation.delegate = self;
    
    return _expandMarkerAnimation;
}

- (CAShapeLayer *)starShapeLayer
{
    
    /// Star Drawing
    UIBezierPath* starPath = [UIBezierPath bezierPath];
    [starPath moveToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 18, CGRectGetMinY(self.bounds) + 29.25)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 20.53, CGRectGetMinY(self.bounds) + 36.52)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 28.22, CGRectGetMinY(self.bounds) + 36.68)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 22.09, CGRectGetMinY(self.bounds) + 41.33)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 24.32, CGRectGetMinY(self.bounds) + 48.7)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 18, CGRectGetMinY(self.bounds) + 44.3)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 11.68, CGRectGetMinY(self.bounds) + 48.7)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 13.91, CGRectGetMinY(self.bounds) + 41.33)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 7.78, CGRectGetMinY(self.bounds) + 36.68)];
    [starPath addLineToPoint: CGPointMake(CGRectGetMinX(self.bounds) + 15.47, CGRectGetMinY(self.bounds) + 36.52)];
    [starPath closePath];
    
    if (!_starShapeLayer) {
        _starShapeLayer = [CAShapeLayer layer];
    }
    
    _starShapeLayer.fillColor = [TOLColor addToFavoritesStarColor].CGColor;
    _starShapeLayer.path = starPath.CGPath;
    
    return _starShapeLayer;
}

- (void)setFavorited:(BOOL)favorited
{
    _favorited = favorited;
    
    if (_favorited) {
        [self slideToOpen];
    }
}

#pragma mark - UIGestureReconizer

- (void)handleOpenPanGesture:(UIPanGestureRecognizer *)panGestureRecognizer
{
    
    CGPoint translation = [panGestureRecognizer translationInView:self.superview];
    
    if(translation.y > kTOLMarkerMaximumHeight) {
        panGestureRecognizer.enabled = NO;
        
        // handleOpenPanGesture it's trigger when it's dragging and when it's finished, to avoid calling everything twice this flag was created.
        if (!self.expandAnimationHasStarted) {
            [self slideToOpen];
        }
        
        return;
    }
    
    switch (panGestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged: {
            // Follow finger drag
            CGFloat longHeight = translation.y + self.initialHeight;
            
            self.frame = CGRectMake(CGRectGetMinX(self.frame),CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), longHeight);
            
            break;
        }
        case UIGestureRecognizerStateEnded: {
            if(translation.y < kTOLMarkerMaximumHeight){
                [self slideToClose];
                
                return;
            }
            
            break;
        }
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
            break;
    }
}

- (void)handleCloseDragGesture:(UIPanGestureRecognizer *)panGestureRecognizer
{
    
    CGPoint translation = [panGestureRecognizer translationInView:self.superview];
    
    if (translation.y > kTOLMinimumDistanceToUnstar) {
        if (!self.expandAnimationHasStarted) {
            [self slideToClose];
        }
        
        return;
    }
    
    switch (panGestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged:
        {
            // Follow finger drag
            CGFloat longHeight = translation.y + kTOLMarkerExpandedHeight;
            
            self.frame = CGRectMake(CGRectGetMinX(self.frame),CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), longHeight);
            
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
            break;
    }
    
}

#pragma mark - Animations

- (void)slideToOpen
{
    // Clean up existing recognizers
    for (UIGestureRecognizer *recognizer in [self.gestureRecognizers copy]) {
        recognizer.enabled = NO;
        [self removeGestureRecognizer:recognizer];
    }
 
    
    [self.starShapeLayer removeAnimationForKey:@"opacity"];
    
    self.appearingStarAnimation.toValue = @(1);
    [self.starShapeLayer pop_addAnimation:self.appearingStarAnimation forKey:@"opacity"];
    
    [self pop_removeAnimationForKey:@"frame"];
    
    self.expandMarkerAnimation.name = kTOLSlidingToOpenAnimationIdentifier;
    self.expandMarkerAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(CGRectGetMinX(self.frame),CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), kTOLMarkerExpandedHeight)];
    
    [self pop_addAnimation:self.expandMarkerAnimation forKey:@"frame"];
    
    self.expandAnimationHasStarted = YES;
    
    UIPanGestureRecognizer *dragGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleCloseDragGesture:)];
    [self addGestureRecognizer:dragGestureRecognizer];

}

- (void)slideToClose
{
    self.expandAnimationHasStarted = YES;
    
    // Clean up existing recognizers
    for (UIGestureRecognizer *recognizer in [self.gestureRecognizers copy]) {
        recognizer.enabled = NO;
        [self removeGestureRecognizer:recognizer];
    }
    
    [self.starShapeLayer removeAnimationForKey:@"opacity"];
    
    self.appearingStarAnimation.toValue = @(0);
    
    [self.starShapeLayer pop_addAnimation:self.appearingStarAnimation forKey:@"opacity"];
    
    [self pop_removeAnimationForKey:@"frame"];
    
    self.expandMarkerAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), self.initialHeight)];
    [self pop_addAnimation:self.expandMarkerAnimation forKey:@"frame"];
    self.expandMarkerAnimation.name = kTOLSlidingToCloseAnimationIdentifier;

    UIPanGestureRecognizer *dragGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleOpenPanGesture:)];
    [self addGestureRecognizer:dragGestureRecognizer];
}

#pragma mark - POPAnimationDelegate

- (void)pop_animationDidStop:(POPAnimation *)animation finished:(BOOL)finished
{
    if ([animation isEqual:self.expandMarkerAnimation]) {
        if ([animation.name isEqualToString:kTOLSlidingToOpenAnimationIdentifier]) {
            
            if (!self.isFavorited) {
                [self.pullToFavoriteDelegate addToFavorites];
            }
        } else if ([animation.name isEqualToString:kTOLSlidingToCloseAnimationIdentifier]) {
            if (self.isFavorited) {
                [self.pullToFavoriteDelegate removeFromFavorites];
            }
        }

        self.expandAnimationHasStarted = NO;
    
        // Had to nil out the animation object, the animation got confused - next time I don't use as property
        self.expandMarkerAnimation = nil;
    }
    
}


@end
