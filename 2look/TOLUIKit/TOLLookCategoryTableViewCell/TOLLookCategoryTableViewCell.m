//
//  TOLLookCategoriesTableViewCell.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLLookCategoryTableViewCell.h"
#import "LookCategory.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation TOLLookCategoryTableViewCell

- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    
    self.lookCategoryImageView.layer.cornerRadius = CGRectGetHeight(self.lookCategoryImageView.frame) / 2;
    self.lookCategoryImageView.clipsToBounds = YES;
    self.lookCategoryImageView.layer.borderWidth = 1.0;
    self.lookCategoryImageView.backgroundColor = [TOLHelpers randomThemeColor];
    
    self.lookCategoryName.textAlignment = NSTextAlignmentCenter;
    self.numberOfEntriesLabel.textAlignment = NSTextAlignmentCenter;
}

-(void)setLookCategory:(LookCategory *)lookCategory
{
    [self.lookCategoryImageView sd_setImageWithURL:[NSURL URLWithString:lookCategory.thumbURL] placeholderImage:nil];
    self.numberOfEntriesLabel.text = [NSString stringWithFormat:@"%@ entries", lookCategory.entries];
    self.lookCategoryName.text = lookCategory.title ? lookCategory.title : lookCategory.name; //TODO: Fix this <-
    self.lookCategoryId = lookCategory.objectID;    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
