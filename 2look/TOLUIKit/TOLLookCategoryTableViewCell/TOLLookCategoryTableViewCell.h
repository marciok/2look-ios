//
//  TOLLookCategoriesTableViewCell.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LookCategory;

@interface TOLLookCategoryTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *lookCategoryImageView;
@property (nonatomic, weak) IBOutlet UILabel *lookCategoryName;
@property (nonatomic, weak) IBOutlet UILabel *numberOfEntriesLabel;

@property (nonatomic, weak) LookCategory *lookCategory;
@property (nonatomic, strong) NSManagedObjectID *lookCategoryId;

@end
