//
//  TOLLookCollectionViewCell.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLLookCollectionViewCell.h"
#import "LookCategory.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TOLLookCollectionViewCell ()

@end

@implementation TOLLookCollectionViewCell


- (void)awakeFromNib
{
    self.lookImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.lookImageView.backgroundColor = [TOLHelpers randomThemeColor];
    self.lookImageView.clipsToBounds = YES;
   
    self.lookSourceThumb.backgroundColor = [TOLColor lightGrayColorHalfAlpha];
    self.lookSourceThumb.clipsToBounds = YES;
}

- (void)setLookId:(NSManagedObjectID *)lookId
{
    _lookId = lookId;
    
    
    NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
    Look *look = (Look *)[mainContext objectWithID:_lookId];

    [self.lookImageView sd_setImageWithURL:[NSURL URLWithString:look.pictureURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
//    CGFloat height = floorf(self.lookImageView.image.size.height * (self.contentView.bounds.size.width / self.lookImageView.image.size.width));
//    
//    CGSize final = CGSizeMake(self.contentView.bounds.size.width, height + arc4random_uniform(100));
//    
//    UIGraphicsBeginImageContextWithOptions(final, NO, 0.0f);
//    
//    //draw
//    [self.lookImageView.image drawInRect:CGRectMake(0.0f, 0.0f, final.width, final.height)];
//    
//    //capture resultant image
//    UIImage *imageFinal = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
//    
//    CGRect bounds;
//    
//    bounds.origin = CGPointZero;
//    bounds.size = imageFinal.size;
//    
//    self.lookImageView.frame = bounds;
//    self.lookImageView.image = imageFinal;
//    
//    [self.contentView setNeedsLayout];
//    [self.contentView layoutIfNeeded];
    
    
    //    [self.lookSourceNameButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    //    self.lookSourceNameButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    
    
    LookCategory *supplierCategory = look.supplier;
    self.lookSourceName.text = [NSString stringWithFormat:@"@%@", supplierCategory.name];
    self.lookSourceName.attributedText = [[NSAttributedString alloc] initWithString:self.lookSourceName.text attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : [UIColor grayColor]}];
    
    [self.lookSourceThumb sd_setImageWithURL:[NSURL URLWithString:supplierCategory.thumbURL]];
}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    [self.contentView layoutIfNeeded];
//}

@end
