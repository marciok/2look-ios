//
//  TOLLookCollectionViewCell.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Look.h"

@protocol TOLLookCollectionViewCellDelegate <NSObject>

- (void)sizeWithImageLoaded:(CGSize)size;

@end

@interface TOLLookCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *lookSourceThumb;
@property (nonatomic, weak) IBOutlet UILabel *lookSourceName;
@property (nonatomic, weak) IBOutlet UIImageView *lookImageView;

@property (nonatomic, strong) NSManagedObjectID *lookId;

@property (nonatomic, weak) id<TOLLookCollectionViewCellDelegate> delegate;

@end
