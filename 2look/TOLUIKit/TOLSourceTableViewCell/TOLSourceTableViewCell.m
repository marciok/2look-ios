//
//  TOLSourceTableViewCell.m
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLSourceTableViewCell.h"
#import "LookCategory.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TOLSourceTableViewCell ()

@end

@implementation TOLSourceTableViewCell


- (void)awakeFromNib
{
    self.backgroundColor = [TOLColor ultraLightGrayColor];
    
    self.sourceImage.layer.cornerRadius = CGRectGetHeight(self.sourceImage.frame) / 2;
    self.sourceImage.layer.masksToBounds = YES;
    self.sourceImage.layer.borderColor = [[TOLColor darkGreenColor] CGColor];
    self.sourceImage.layer.borderWidth = 1.0;
    self.sourceImage.backgroundColor = [TOLHelpers randomThemeColor];
}

-(void)setLookCategoryId:(NSManagedObjectID *)lookCategoryId
{
    _lookCategoryId = lookCategoryId;
    
    NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
    
    LookCategory *lookCategory = (LookCategory *)[mainContext objectWithID:_lookCategoryId];
    
    [self.sourceImage sd_setImageWithURL:[NSURL URLWithString:lookCategory.thumbURL]];
    self.sourceNameLabel.text = lookCategory.title ?: lookCategory.name;
    self.sourceCountLabel.text = [NSString stringWithFormat:@"%@", lookCategory.entries];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIEdgeInsets)layoutMargins {
    
    return UIEdgeInsetsZero;
}

@end
