//
//  TOLSourceTableViewCell.h
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  LookCategory;

@interface TOLSourceTableViewCell : UITableViewCell

@property (nonatomic, strong) NSManagedObjectID *lookCategoryId;
@property (nonatomic, weak) LookCategory *lookCategory;

@property (nonatomic, weak) IBOutlet UIImageView *sourceImage;
@property (nonatomic, weak) IBOutlet UILabel *sourceNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *sourceCountLabel;

@end
