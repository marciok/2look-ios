//
//  TOLMenuBarButtonItem.h
//  2look
//
//  Created by Marcio Klepacz on 25/12/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TOLMenuBarButton;

@interface TOLMenuBarButtonItem : UIBarButtonItem

+ (instancetype)button;

// Custom target action.
- (void)addTarget:(id)target action:(SEL)action;


@end
