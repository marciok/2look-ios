//
//  TOLMenuBarButtonItem.m
//  2look
//
//  Created by Marcio Klepacz on 25/12/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLMenuBarButtonItem.h"
#import "TOLMenuBarButton.h"

@interface TOLMenuBarButtonItem ()

@property (nonatomic, strong) TOLMenuBarButton *baseButton;

@end

@implementation TOLMenuBarButtonItem

+ (instancetype)button
{
    // Needed to touch work;
    TOLMenuBarButton *baseButton = [TOLMenuBarButton button];
    TOLMenuBarButtonItem *buttonItem = [[TOLMenuBarButtonItem alloc] initWithCustomView:baseButton];
    
    buttonItem.baseButton = baseButton;
    
    return buttonItem;
}

- (void)addTarget:(id)target action:(SEL)action
{
    [self.baseButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}



@end
