//
//  UIFont+TOLFont.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "UIFont+TOLFont.h"

@implementation UIFont (TOLFont)

+ (UIFont *)fontForTitle
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:24.0f];
}

+ (UIFont *)fontForSubtitle
{
    return [UIFont fontWithName:@"HelveticaNeue-light" size:17.0f];
}

@end
