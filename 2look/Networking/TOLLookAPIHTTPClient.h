//
//  ToLookAPIHTTPClient.h
//  2look
//
//  Created by Marcio Klepacz on 5/16/14.
//  Copyright (c) 2014 tolook. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface TOLLookAPIHTTPClient : AFHTTPSessionManager

+ (instancetype)v1SharedClient;

- (void)fetchLatestLooksWithMaxId:(NSString *)maxId withCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock;

- (void)fetchLatestLooksWithMaxId:(NSString *)maxId forLookCategoryNameOnTOLAPI:(NSString *)lookCategoryName AndId:(NSString *)idOnTOLAPI withCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock;

- (void)fetchLatestLooksWithCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock;

- (void)allCategoriesForType:(TOLLookCategoryType)lookCategoryType withCompletion:(void (^)(NSArray *lookCategories, NSError *error))completionBlock;

- (void)fetchLooksDetailsForLookId:(NSString *)lookIdOnAPI withCompletionBlock:(void (^)(NSError *error, NSDictionary *lookDetails))completionBlock;

- (void)createUserWithCompletion:(void(^)(NSString *idOnAPICreated, NSError *error))completionBlock;

@end
