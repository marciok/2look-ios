//
//  ToLookAPIHTTPClient.m
//  2look
//
//  Created by Marcio Klepacz on 5/16/14.
//  Copyright (c) 2014 tolook. All rights reserved.
//

#import "TOLLookAPIHTTPClient.h"
#import "Look.h"

static NSString *const kToLookV1APIBaseURL = @"http://tolook.herokuapp.com/1/";
static NSString *const kTOLAPIFileFormat = @"json";

@implementation TOLLookAPIHTTPClient

+ (instancetype)v1SharedClient
{
    static TOLLookAPIHTTPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kToLookV1APIBaseURL]];
    });
    
    return _sharedClient;
}

- (void)fetchLatestLooksWithCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock
{
    [self fetchLatestLooksWithMaxId:nil withCompletion:completionBlock];
}

//TODO: Change for passing the error instead of the success
- (void)fetchLatestLooksWithMaxId:(NSString *)maxId withCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock
{
    NSDictionary *parameters = maxId ? @{@"max_id" : maxId} : nil;
    
    [self GET:[NSString stringWithFormat:@"looks.%@", kTOLAPIFileFormat] parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (completionBlock) {
            completionBlock(responseObject[@"looks"],YES);
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error on downloadLatestLooksWithMaxId: %@", error);
        
        if (completionBlock) {
            completionBlock(nil,NO);
        }
        
    }];
}

- (void)fetchLatestLooksWithMaxId:(NSString *)maxId forLookCategoryNameOnTOLAPI:(NSString *)lookCategoryName AndId:(NSString *)idOnTOLAPI withCompletion:(void (^)(NSArray *looksFromAPI, BOOL success))completionBlock
{
    NSDictionary *parameters = maxId ? @{@"max_id" : maxId} : nil;
    
    [self GET:[NSString stringWithFormat:@"%@/%@/looks.%@",lookCategoryName, idOnTOLAPI, kTOLAPIFileFormat] parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (completionBlock) {
            completionBlock(responseObject[@"looks"],YES);
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error on downloadLatestLooksWithMaxId: %@", error);
        
        if (completionBlock) {
            completionBlock(nil,NO);
        }
        
    }];
}

- (void)allCategoriesForType:(TOLLookCategoryType)lookCategoryType withCompletion:(void (^)(NSArray *lookCategories, NSError *error))completionBlock
{
    NSString *typeNameOnAPI = [TOLHelpers nameOnTOLAPITForLookCategoryType:lookCategoryType];

    if (!typeNameOnAPI) {
        return;
    }

    [self GET:[NSString stringWithFormat:@"%@.json", typeNameOnAPI] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        if (completionBlock) {
            completionBlock(responseObject[typeNameOnAPI],nil);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (completionBlock) {
            completionBlock(nil, error);
        }
    }];
    
}

- (void)fetchLooksDetailsForLookId:(NSString *)lookIdOnAPI withCompletionBlock:(void (^)(NSError *error, NSDictionary *lookDetails))completionBlock
{
    NSParameterAssert(lookIdOnAPI.length > 0);
    [self GET:[NSString stringWithFormat:@"looks/%@.%@", lookIdOnAPI, kTOLAPIFileFormat] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (completionBlock) {
            completionBlock(nil, responseObject[@"look"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (completionBlock) {
            completionBlock(error, nil);
        }
    }];
    
}

- (void)createUserWithCompletion:(void(^)(NSString *idOnAPICreated, NSError *error))completionBlock
{
    NSParameterAssert(completionBlock);
    
    [self POST:[NSString stringWithFormat:@"users.%@", kTOLAPIFileFormat] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *userDictionary = responseObject[@"user"];
        
        completionBlock(userDictionary[@"id"], nil);

        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionBlock(nil, error);
    }];
}

@end
