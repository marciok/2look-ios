//
//  TOLDataModel.h
//  2look
//
//  Created by Marcio Klepacz on 8/9/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <MagicalRecord/CoreData+MagicalRecord.h>
#import "NSManagedObjectContext+TOLDataModel.h"
#import "NSManagedObject+TOLDataModel.h"