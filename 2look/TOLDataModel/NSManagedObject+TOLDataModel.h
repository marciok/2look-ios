//
//  NSManagedObject+TOLDataModel.h
//  2look
//
//  Created by Marcio Klepacz on 8/9/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (TOLDataModel)

+ (id)createInContext:(NSManagedObjectContext *)context;

+ (BOOL)hasAtLeastOneEntity;

+ (NSUInteger)count;

@end
