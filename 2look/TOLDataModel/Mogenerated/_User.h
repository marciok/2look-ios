// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.h instead.

#import <CoreData/CoreData.h>

extern const struct UserAttributes {
	__unsafe_unretained NSString *idOnServer;
	__unsafe_unretained NSString *isCurrent;
	__unsafe_unretained NSString *userId;
} UserAttributes;

extern const struct UserRelationships {
	__unsafe_unretained NSString *favorites;
} UserRelationships;

@class Look;

@interface UserID : NSManagedObjectID {}
@end

@interface _User : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserID* objectID;

@property (nonatomic, strong) NSString* idOnServer;

//- (BOOL)validateIdOnServer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isCurrent;

@property (atomic) BOOL isCurrentValue;
- (BOOL)isCurrentValue;
- (void)setIsCurrentValue:(BOOL)value_;

//- (BOOL)validateIsCurrent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* userId;

@property (atomic) int64_t userIdValue;
- (int64_t)userIdValue;
- (void)setUserIdValue:(int64_t)value_;

//- (BOOL)validateUserId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *favorites;

- (NSMutableSet*)favoritesSet;

@end

@interface _User (FavoritesCoreDataGeneratedAccessors)
- (void)addFavorites:(NSSet*)value_;
- (void)removeFavorites:(NSSet*)value_;
- (void)addFavoritesObject:(Look*)value_;
- (void)removeFavoritesObject:(Look*)value_;

@end

@interface _User (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveIdOnServer;
- (void)setPrimitiveIdOnServer:(NSString*)value;

- (NSNumber*)primitiveIsCurrent;
- (void)setPrimitiveIsCurrent:(NSNumber*)value;

- (BOOL)primitiveIsCurrentValue;
- (void)setPrimitiveIsCurrentValue:(BOOL)value_;

- (NSNumber*)primitiveUserId;
- (void)setPrimitiveUserId:(NSNumber*)value;

- (int64_t)primitiveUserIdValue;
- (void)setPrimitiveUserIdValue:(int64_t)value_;

- (NSMutableSet*)primitiveFavorites;
- (void)setPrimitiveFavorites:(NSMutableSet*)value;

@end
