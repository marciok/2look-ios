// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LookCategory.m instead.

#import "_LookCategory.h"

const struct LookCategoryAttributes LookCategoryAttributes = {
	.entries = @"entries",
	.idOnServer = @"idOnServer",
	.name = @"name",
	.supplier = @"supplier",
	.thumbURL = @"thumbURL",
	.title = @"title",
	.type = @"type",
};

const struct LookCategoryRelationships LookCategoryRelationships = {
	.looks = @"looks",
};

@implementation LookCategoryID
@end

@implementation _LookCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LookCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LookCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LookCategory" inManagedObjectContext:moc_];
}

- (LookCategoryID*)objectID {
	return (LookCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"entriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"entries"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"supplierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"supplier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"typeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"type"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic entries;

- (int32_t)entriesValue {
	NSNumber *result = [self entries];
	return [result intValue];
}

- (void)setEntriesValue:(int32_t)value_ {
	[self setEntries:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveEntriesValue {
	NSNumber *result = [self primitiveEntries];
	return [result intValue];
}

- (void)setPrimitiveEntriesValue:(int32_t)value_ {
	[self setPrimitiveEntries:[NSNumber numberWithInt:value_]];
}

@dynamic idOnServer;

@dynamic name;

@dynamic supplier;

- (int64_t)supplierValue {
	NSNumber *result = [self supplier];
	return [result longLongValue];
}

- (void)setSupplierValue:(int64_t)value_ {
	[self setSupplier:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveSupplierValue {
	NSNumber *result = [self primitiveSupplier];
	return [result longLongValue];
}

- (void)setPrimitiveSupplierValue:(int64_t)value_ {
	[self setPrimitiveSupplier:[NSNumber numberWithLongLong:value_]];
}

@dynamic thumbURL;

@dynamic title;

@dynamic type;

- (int16_t)typeValue {
	NSNumber *result = [self type];
	return [result shortValue];
}

- (void)setTypeValue:(int16_t)value_ {
	[self setType:[NSNumber numberWithShort:value_]];
}

@dynamic looks;

- (NSMutableSet*)looksSet {
	[self willAccessValueForKey:@"looks"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"looks"];

	[self didAccessValueForKey:@"looks"];
	return result;
}

@end

