// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.m instead.

#import "_User.h"

const struct UserAttributes UserAttributes = {
	.idOnServer = @"idOnServer",
	.isCurrent = @"isCurrent",
	.userId = @"userId",
};

const struct UserRelationships UserRelationships = {
	.favorites = @"favorites",
};

@implementation UserID
@end

@implementation _User

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"User";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"User" inManagedObjectContext:moc_];
}

- (UserID*)objectID {
	return (UserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isCurrentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCurrent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic idOnServer;

@dynamic isCurrent;

- (BOOL)isCurrentValue {
	NSNumber *result = [self isCurrent];
	return [result boolValue];
}

- (void)setIsCurrentValue:(BOOL)value_ {
	[self setIsCurrent:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsCurrentValue {
	NSNumber *result = [self primitiveIsCurrent];
	return [result boolValue];
}

- (void)setPrimitiveIsCurrentValue:(BOOL)value_ {
	[self setPrimitiveIsCurrent:[NSNumber numberWithBool:value_]];
}

@dynamic userId;

- (int64_t)userIdValue {
	NSNumber *result = [self userId];
	return [result longLongValue];
}

- (void)setUserIdValue:(int64_t)value_ {
	[self setUserId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveUserIdValue {
	NSNumber *result = [self primitiveUserId];
	return [result longLongValue];
}

- (void)setPrimitiveUserIdValue:(int64_t)value_ {
	[self setPrimitiveUserId:[NSNumber numberWithLongLong:value_]];
}

@dynamic favorites;

- (NSMutableSet*)favoritesSet {
	[self willAccessValueForKey:@"favorites"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"favorites"];

	[self didAccessValueForKey:@"favorites"];
	return result;
}

@end

