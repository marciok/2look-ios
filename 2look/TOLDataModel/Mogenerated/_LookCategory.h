// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LookCategory.h instead.

#import <CoreData/CoreData.h>

extern const struct LookCategoryAttributes {
	__unsafe_unretained NSString *entries;
	__unsafe_unretained NSString *idOnServer;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *supplier;
	__unsafe_unretained NSString *thumbURL;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *type;
} LookCategoryAttributes;

extern const struct LookCategoryRelationships {
	__unsafe_unretained NSString *looks;
} LookCategoryRelationships;

@class Look;

@interface LookCategoryID : NSManagedObjectID {}
@end

@interface _LookCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LookCategoryID* objectID;

@property (nonatomic, strong) NSNumber* entries;

@property (atomic) int32_t entriesValue;
- (int32_t)entriesValue;
- (void)setEntriesValue:(int32_t)value_;

//- (BOOL)validateEntries:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* idOnServer;

//- (BOOL)validateIdOnServer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* supplier;

@property (atomic) int64_t supplierValue;
- (int64_t)supplierValue;
- (void)setSupplierValue:(int64_t)value_;

//- (BOOL)validateSupplier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* thumbURL;

//- (BOOL)validateThumbURL:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* type;

@property (atomic) int16_t typeValue;
- (int16_t)typeValue;
- (void)setTypeValue:(int16_t)value_;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *looks;

- (NSMutableSet*)looksSet;

@end

@interface _LookCategory (LooksCoreDataGeneratedAccessors)
- (void)addLooks:(NSSet*)value_;
- (void)removeLooks:(NSSet*)value_;
- (void)addLooksObject:(Look*)value_;
- (void)removeLooksObject:(Look*)value_;

@end

@interface _LookCategory (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveEntries;
- (void)setPrimitiveEntries:(NSNumber*)value;

- (int32_t)primitiveEntriesValue;
- (void)setPrimitiveEntriesValue:(int32_t)value_;

- (NSString*)primitiveIdOnServer;
- (void)setPrimitiveIdOnServer:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveSupplier;
- (void)setPrimitiveSupplier:(NSNumber*)value;

- (int64_t)primitiveSupplierValue;
- (void)setPrimitiveSupplierValue:(int64_t)value_;

- (NSString*)primitiveThumbURL;
- (void)setPrimitiveThumbURL:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSMutableSet*)primitiveLooks;
- (void)setPrimitiveLooks:(NSMutableSet*)value;

@end
