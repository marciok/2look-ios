// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Look.m instead.

#import "_Look.h"

const struct LookAttributes LookAttributes = {
	.createdAt = @"createdAt",
	.favorited = @"favorited",
	.idOnServer = @"idOnServer",
	.lookId = @"lookId",
	.pictureURL = @"pictureURL",
};

const struct LookRelationships LookRelationships = {
	.lookCategories = @"lookCategories",
	.userFavorited = @"userFavorited",
};

@implementation LookID
@end

@implementation _Look

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Look" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Look";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Look" inManagedObjectContext:moc_];
}

- (LookID*)objectID {
	return (LookID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"favoritedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"favorited"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"lookIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"lookId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createdAt;

@dynamic favorited;

- (BOOL)favoritedValue {
	NSNumber *result = [self favorited];
	return [result boolValue];
}

- (void)setFavoritedValue:(BOOL)value_ {
	[self setFavorited:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveFavoritedValue {
	NSNumber *result = [self primitiveFavorited];
	return [result boolValue];
}

- (void)setPrimitiveFavoritedValue:(BOOL)value_ {
	[self setPrimitiveFavorited:[NSNumber numberWithBool:value_]];
}

@dynamic idOnServer;

@dynamic lookId;

- (int64_t)lookIdValue {
	NSNumber *result = [self lookId];
	return [result longLongValue];
}

- (void)setLookIdValue:(int64_t)value_ {
	[self setLookId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveLookIdValue {
	NSNumber *result = [self primitiveLookId];
	return [result longLongValue];
}

- (void)setPrimitiveLookIdValue:(int64_t)value_ {
	[self setPrimitiveLookId:[NSNumber numberWithLongLong:value_]];
}

@dynamic pictureURL;

@dynamic lookCategories;

- (NSMutableSet*)lookCategoriesSet {
	[self willAccessValueForKey:@"lookCategories"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"lookCategories"];

	[self didAccessValueForKey:@"lookCategories"];
	return result;
}

@dynamic userFavorited;

- (NSMutableSet*)userFavoritedSet {
	[self willAccessValueForKey:@"userFavorited"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userFavorited"];

	[self didAccessValueForKey:@"userFavorited"];
	return result;
}

@end

