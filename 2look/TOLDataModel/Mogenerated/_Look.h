// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Look.h instead.

#import <CoreData/CoreData.h>

extern const struct LookAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *favorited;
	__unsafe_unretained NSString *idOnServer;
	__unsafe_unretained NSString *lookId;
	__unsafe_unretained NSString *pictureURL;
} LookAttributes;

extern const struct LookRelationships {
	__unsafe_unretained NSString *lookCategories;
	__unsafe_unretained NSString *userFavorited;
} LookRelationships;

@class LookCategory;
@class User;

@interface LookID : NSManagedObjectID {}
@end

@interface _Look : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LookID* objectID;

@property (nonatomic, strong) NSDate* createdAt;

//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* favorited;

@property (atomic) BOOL favoritedValue;
- (BOOL)favoritedValue;
- (void)setFavoritedValue:(BOOL)value_;

//- (BOOL)validateFavorited:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* idOnServer;

//- (BOOL)validateIdOnServer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* lookId;

@property (atomic) int64_t lookIdValue;
- (int64_t)lookIdValue;
- (void)setLookIdValue:(int64_t)value_;

//- (BOOL)validateLookId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* pictureURL;

//- (BOOL)validatePictureURL:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *lookCategories;

- (NSMutableSet*)lookCategoriesSet;

@property (nonatomic, strong) NSSet *userFavorited;

- (NSMutableSet*)userFavoritedSet;

@end

@interface _Look (LookCategoriesCoreDataGeneratedAccessors)
- (void)addLookCategories:(NSSet*)value_;
- (void)removeLookCategories:(NSSet*)value_;
- (void)addLookCategoriesObject:(LookCategory*)value_;
- (void)removeLookCategoriesObject:(LookCategory*)value_;

@end

@interface _Look (UserFavoritedCoreDataGeneratedAccessors)
- (void)addUserFavorited:(NSSet*)value_;
- (void)removeUserFavorited:(NSSet*)value_;
- (void)addUserFavoritedObject:(User*)value_;
- (void)removeUserFavoritedObject:(User*)value_;

@end

@interface _Look (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;

- (NSNumber*)primitiveFavorited;
- (void)setPrimitiveFavorited:(NSNumber*)value;

- (BOOL)primitiveFavoritedValue;
- (void)setPrimitiveFavoritedValue:(BOOL)value_;

- (NSString*)primitiveIdOnServer;
- (void)setPrimitiveIdOnServer:(NSString*)value;

- (NSNumber*)primitiveLookId;
- (void)setPrimitiveLookId:(NSNumber*)value;

- (int64_t)primitiveLookIdValue;
- (void)setPrimitiveLookIdValue:(int64_t)value_;

- (NSString*)primitivePictureURL;
- (void)setPrimitivePictureURL:(NSString*)value;

- (NSMutableSet*)primitiveLookCategories;
- (void)setPrimitiveLookCategories:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUserFavorited;
- (void)setPrimitiveUserFavorited:(NSMutableSet*)value;

@end
