//
//  NSManagedObjectContext+TOLDataModel.m
//  2look
//
//  Created by Marcio Klepacz on 8/9/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "NSManagedObjectContext+TOLDataModel.h"

@implementation NSManagedObjectContext (TOLDataModel)

+ (NSManagedObjectContext *)mainContext
{
    return [self MR_defaultContext];
}

- (void)saveAndWait
{
    [self MR_saveToPersistentStoreAndWait];
}

@end
