#import "_LookCategory.h"

@interface LookCategory : _LookCategory {}

+ (LookCategory *)createOrUpdateLookCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context;

+ (LookCategory *)createCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context;

+ (NSFetchedResultsController *)fetchAllForType:(TOLLookCategoryType)typeToFetch;

- (LookCategory *)updateCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context;


@end
