#import "User.h"
#import "Look.h"


@interface User ()

// Private interface goes here.

@end


@implementation User

+ (User *)createUserWithIdOnAPI:(NSString *)idOnAPI inContext:(NSManagedObjectContext *)context
{
    User *user = [User createInContext:context];
    user.idOnServer = idOnAPI;
    
    [context saveAndWait];
    
    return user;
}

+ (User *)currentUserInContext:(NSManagedObjectContext *)context
{
    return [User MR_findFirstByAttribute:@"isCurrent" withValue:@(YES) inContext:context];
}

+ (void)setCurrentUser:(User *)user inContext:(NSManagedObjectContext *)context
{
    User *currentUser = [User currentUserInContext:context];

    if (currentUser) {
        currentUser.isCurrent = @(NO);
    }

    user.isCurrent = @(YES);
    
    [context saveAndWait];
}

- (NSFetchedResultsController *)fetchFavoritesInContext:(NSManagedObjectContext *)context
{
    User *user = (User *)[context objectWithID:[self objectID]];
    NSPredicate *findLooksFavortiedByUser = [NSPredicate predicateWithFormat:@"ANY userFavorited == %@", user];
    
    return [Look MR_fetchAllGroupedBy:nil withPredicate:findLooksFavortiedByUser sortedBy:@"createdAt" ascending:YES inContext:context];
}



@end
