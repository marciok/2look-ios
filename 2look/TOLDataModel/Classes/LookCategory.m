#import "LookCategory.h"


@interface LookCategory ()

// Private interface goes here.

@end


@implementation LookCategory

+ (LookCategory *)createOrUpdateLookCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context
{
    NSParameterAssert(parameters[@"type"]);
    NSParameterAssert(parameters[@"idOnServer"]);
    NSParameterAssert(context);
    
    NSPredicate *findLookCategoryByServerIdPredicate = [NSPredicate predicateWithFormat:@"type == %@ AND idOnServer == %@", parameters[@"type"], parameters[@"idOnServer"]];
    
    LookCategory *lookCategory = [LookCategory MR_findFirstWithPredicate:findLookCategoryByServerIdPredicate inContext:context];
    
    if (!lookCategory) {
        lookCategory = [self createCategoryWithParameters:parameters inContext:context];
    }
    else {
        lookCategory = [lookCategory updateCategoryWithParameters:parameters inContext:context];
    }

    return lookCategory;
}

+ (LookCategory *)createCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context
{
    LookCategory *lookCategory = [LookCategory createInContext:context];
    
    [lookCategory updateCategoryWithParameters:parameters inContext:context];
    
    return lookCategory;
}

- (LookCategory *)updateCategoryWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context
{
    LookCategory *selfLookCategory = (LookCategory *)[context objectWithID:[self objectID]];
    
    selfLookCategory.type = parameters[@"type"] ?  parameters[@"type"] : selfLookCategory.type;
    selfLookCategory.idOnServer = parameters[@"idOnServer"] ? parameters[@"idOnServer"] : selfLookCategory.idOnServer;
    selfLookCategory.entries = parameters[@"entries"] ? parameters[@"entries"] : selfLookCategory.entries;
    selfLookCategory.name = parameters[@"name"] ? parameters[@"name"] : selfLookCategory.name;
    selfLookCategory.title = parameters[@"title"] ? parameters[@"title"] : selfLookCategory.title;
    selfLookCategory.thumbURL = parameters[@"thumbURL"] ? parameters[@"thumbURL"] : selfLookCategory.thumbURL;
    
    return selfLookCategory;
}


+ (NSFetchedResultsController *)fetchAllForType:(TOLLookCategoryType)typeToFetch
{
    NSPredicate *fetchByTypePredicate;
    
    if (typeToFetch != TOLLookCategoryTypeAll) {
        fetchByTypePredicate = [NSPredicate predicateWithFormat:@"type == %i", typeToFetch];
    }
    
    return [LookCategory MR_fetchAllGroupedBy:nil withPredicate:fetchByTypePredicate sortedBy:@"name" ascending:YES];
}

@end
