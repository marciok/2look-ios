#import "_User.h"

@interface User : _User {}

+ (User *)createUserWithIdOnAPI:(NSString *)idOnAPI inContext:(NSManagedObjectContext *)context;

+ (User *)currentUserInContext:(NSManagedObjectContext *)context;

+ (void)setCurrentUser:(User *)user inContext:(NSManagedObjectContext *)context;

- (NSFetchedResultsController *)fetchFavoritesInContext:(NSManagedObjectContext *)context;

@end
