#import "Look.h"
#import "LookCategory.h"
#import "TOLLookAPIHTTPClient.h"

@interface Look ()

@end

@implementation Look

+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withCompletion:(void(^)(NSError *error))completionBlock
{
    [self addLatestLooksFromTOLAPIInContext:context savingEach:savingEachLook withMaxIdOnAPI:nil withCompletion:completionBlock];
}

+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withMaxIdOnAPI:(NSString *)maxIdOnAPI forLookCategoryId:(NSManagedObjectID *)lookCategoryId withCompletion:(void(^)(NSError *error))completionBlock
{
    if (!lookCategoryId) {
        return [self addLatestLooksFromTOLAPIInContext:context savingEach:savingEachLook withMaxIdOnAPI:maxIdOnAPI withCompletion:completionBlock];
    }
    
    LookCategory *lookCategory = (LookCategory *)[context objectWithID:lookCategoryId];
    NSString *idOnTOLAPI = lookCategory.idOnServer;
    
    TOLLookCategoryType lookCategoryType = (TOLLookCategoryType)[lookCategory.type integerValue];
    
    NSString *categoryNameOnTOLAPI = [TOLHelpers nameOnTOLAPITForLookCategoryType:lookCategoryType];
    
    [[TOLLookAPIHTTPClient v1SharedClient] fetchLatestLooksWithMaxId:maxIdOnAPI forLookCategoryNameOnTOLAPI:categoryNameOnTOLAPI AndId:idOnTOLAPI withCompletion:^(NSArray *looksFromAPI, BOOL success) {
        [looksFromAPI enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSString *pictureURL = obj[@"picture"];
            NSString *lookIdOnSever = obj[@"id"];
            
            NSDictionary *lookCategoryParameters = @{@"idOnServer" : obj[@"source"][@"id"],
                                                     @"name" : obj[@"source"][@"name"],
                                                     @"thumbURL" : obj[@"source"][@"thumb"],
                                                     @"type" : @(TOLLookCategoryTypeSupplier)};
            
            LookCategory *supplierCategory = [LookCategory createOrUpdateLookCategoryWithParameters:lookCategoryParameters inContext:context];
            
            NSDictionary *parameters = @{@"pictureURL" : pictureURL,
                                         @"idOnServer" : lookIdOnSever};
            
            Look *look = [Look createLookWithParameters:parameters inContext:context];
            
            [look addLookCategoriesObject:supplierCategory];
            
            NSSet *lookCategoriesSet = [NSSet setWithObjects:supplierCategory, lookCategory, nil];
            
            [look addLookCategories:lookCategoriesSet];
            if (savingEachLook) {
                [context saveAndWait];
            }
        }];
        
        if (!savingEachLook) {
            [context saveAndWait];
        }
        
        if (completionBlock) {
            completionBlock(nil);
        }
    }];
}


+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withMaxIdOnAPI:(NSString *)maxIdOnAPI withCompletion:(void(^)(NSError *error))completionBlock
{
    [[TOLLookAPIHTTPClient v1SharedClient] fetchLatestLooksWithMaxId:maxIdOnAPI withCompletion:^(NSArray *looksFromAPI, BOOL success) {
        
        [looksFromAPI enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSString *pictureURL = obj[@"picture"];
            NSString *lookIdOnSever = obj[@"id"];
            
            NSDictionary *lookCategoryParameters = @{@"idOnServer" : obj[@"source"][@"id"],
                                                     @"name" : obj[@"source"][@"name"],
                                                     @"thumbURL" : obj[@"source"][@"thumb"],
                                                     @"type" : @(TOLLookCategoryTypeSupplier)};
            
            LookCategory *supplierCategory = [LookCategory createOrUpdateLookCategoryWithParameters:lookCategoryParameters inContext:context];
            
            NSDictionary *parameters = @{@"pictureURL" : pictureURL,
                                         @"idOnServer" : lookIdOnSever};
            
            Look *look = [Look createLookWithParameters:parameters inContext:context];
            
            [look addLookCategoriesObject:supplierCategory];
            
            if (savingEachLook) {
                [context saveAndWait];
            }
        }];
        
        if (!savingEachLook) {
            [context saveAndWait];
        }
        
        if (completionBlock) {
            completionBlock(nil);
        }
    }];
}

+ (NSFetchedResultsController *)fetchLooksInContext:(NSManagedObjectContext *)context
{
    return [self fetchLooksForCategory:nil inContext:context];
}


+ (NSFetchedResultsController *)fetchLooksForCategory:(NSString *)categoryName inContext:(NSManagedObjectContext *)context
{
    NSFetchedResultsController *controller = [Look MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"createdAt" ascending:YES];
    
    return controller;
}

+ (NSFetchedResultsController *)fetchLooksForCategoryId:(NSManagedObjectID *)lookCategoryId inContext:(NSManagedObjectContext *)context
{
    LookCategory *lookCategory = (LookCategory *)[context objectWithID:lookCategoryId];
    NSPredicate *findLooksWithCategoryPredicate = [NSPredicate predicateWithFormat:@"ANY lookCategories == %@", lookCategory];

    
    NSFetchedResultsController *controller = [Look MR_fetchAllGroupedBy:nil withPredicate:findLooksWithCategoryPredicate sortedBy:@"createdAt" ascending:YES inContext:context];
    
    return controller;
}

+ (Look *)createLookWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context
{
    Look *look = [Look createInContext:context];
    look.createdAt = [NSDate date];//TODO: Add the actual date
    look.pictureURL = parameters[@"pictureURL"];
    look.idOnServer = parameters[@"idOnServer"];
    
    return look;
}

- (BOOL)favoritedValue
{
    return [[NSNumber numberWithInteger:[[self.userFavorited allObjects] count]] boolValue];
}

- (LookCategory *)supplier
{
    
    NSSet *lookSupplier = [[self valueForKey:@"lookCategories"] filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"type == %@", @(TOLLookCategoryTypeSupplier)]];
    
    return [[lookSupplier allObjects] firstObject];
}

- (void)fetchMoreLookCategoriesWithCompletion:(void(^)(NSArray *lookCategoriesUpdated))completionBlock
{
    NSMutableArray *lookCategoriesWithoutSupplier = [NSMutableArray array];
    NSArray *lookCategories = [LookCategory MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY looks == %@", self]];
    __block BOOL newLooksFromTOLAPI;
    
    [lookCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        LookCategory *lookCategory = obj;
        
        if (![lookCategory.name isEqualToString:self.supplier.name]) {
            [lookCategoriesWithoutSupplier addObject:lookCategory];
        }
    }];
    
    if (completionBlock) {
        completionBlock([[NSArray alloc] initWithArray:lookCategoriesWithoutSupplier]);
    }
    
    [[TOLLookAPIHTTPClient v1SharedClient] fetchLooksDetailsForLookId:self.idOnServer withCompletionBlock:^(NSError *error, NSDictionary *lookDetails) {
        
        NSArray *garmentsFromTOLAPI = lookDetails[@"garments"];
        NSArray *occasionsFromTOLAPI = lookDetails[@"occasions"];
        NSMutableArray *garmentsAndOccasionsFromTOLAPI = [NSMutableArray arrayWithArray:garmentsFromTOLAPI];
        [garmentsAndOccasionsFromTOLAPI addObjectsFromArray:occasionsFromTOLAPI];
        
        NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
        
        for (NSDictionary *lookCategoryOnTOLAPI in garmentsAndOccasionsFromTOLAPI) {

            LookCategory *lookCategoryToAdd = [LookCategory MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name == %@",lookCategoryOnTOLAPI[@"name"]] inContext:mainContext];
            
            NSDictionary *lookCategoryParameters = @{@"name" : lookCategoryOnTOLAPI[@"name"], @"idOnServer" : lookCategoryOnTOLAPI[@"id"]};
            lookCategoryToAdd = lookCategoryToAdd ? : [LookCategory createCategoryWithParameters:lookCategoryParameters inContext:mainContext];
            
            [self addLookCategoriesObject:lookCategoryToAdd];
            
            if (![lookCategoriesWithoutSupplier containsObject:lookCategoryToAdd]) {
                [lookCategoriesWithoutSupplier addObject:lookCategoryToAdd];
                newLooksFromTOLAPI = YES;
            }

        }
        
        if ([lookCategoriesWithoutSupplier count] == 0 || !newLooksFromTOLAPI) {
            return;
        }
        
        [mainContext saveAndWait];
        
        if (completionBlock) {
            completionBlock([[NSArray alloc] initWithArray:lookCategoriesWithoutSupplier]);
        }
        
    }];

    
}


@end

