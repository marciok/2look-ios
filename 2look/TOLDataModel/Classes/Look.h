#import "_Look.h"
@class LookCategory;

@interface Look : _Look {}

@property (nonatomic, strong, readonly) LookCategory *supplier;

+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withMaxIdOnAPI:(NSString *)maxIdOnAPI forLookCategoryId:(NSManagedObjectID *)lookCategoryId withCompletion:(void(^)(NSError *error))completionBlock;

+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withMaxIdOnAPI:(NSString *)maxIdOnAPI withCompletion:(void(^)(NSError *error))completionBlock;

+ (void)addLatestLooksFromTOLAPIInContext:(NSManagedObjectContext *)context savingEach:(BOOL)savingEachLook withCompletion:(void(^)(NSError *error))completionBlock;



+ (NSFetchedResultsController *)fetchLooksInContext:(NSManagedObjectContext *)context;

+ (Look *)createLookWithParameters:(NSDictionary *)parameters inContext:(NSManagedObjectContext *)context;

+ (NSFetchedResultsController *)fetchLooksForCategoryId:(NSManagedObjectID *)lookCategoryId inContext:(NSManagedObjectContext *)context;

- (void)fetchMoreLookCategoriesWithCompletion:(void(^)(NSArray *lookCategoriesUpdated))completionBlock;

- (void)fetchFavoritesForCurrentUserInContext:(NSManagedObjectContext *)context;

@end

