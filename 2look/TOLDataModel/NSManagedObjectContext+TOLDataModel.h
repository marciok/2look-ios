//
//  NSManagedObjectContext+TOLDataModel.h
//  2look
//
//  Created by Marcio Klepacz on 8/9/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (TOLDataModel)

+ (NSManagedObjectContext *)mainContext;

- (void)saveAndWait;

@end
