//
//  NSManagedObject+TOLDataModel.m
//  2look
//
//  Created by Marcio Klepacz on 8/9/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "NSManagedObject+TOLDataModel.h"

@implementation NSManagedObject (TOLDataModel)

+ (id)createInContext:(NSManagedObjectContext *)context
{
    return [self MR_createInContext:context];
}

+ (BOOL)hasAtLeastOneEntity
{
    return [self MR_hasAtLeastOneEntity];
}

+ (NSUInteger)count
{
    return [self MR_countOfEntities];
}

@end
