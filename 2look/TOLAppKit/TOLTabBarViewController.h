//
//  TOLTabBarViewController.h
//  2look
//
//  Created by Marcio Klepacz on 19/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLTabBarViewController : UITabBarController
@end
