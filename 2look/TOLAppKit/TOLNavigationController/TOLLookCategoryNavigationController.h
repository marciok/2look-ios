//
//  TOLLookCategoryNavigationController.h
//  2look
//
//  Created by Marcio Klepacz on 12/03/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLLookCategoryNavigationController : UINavigationController

@end
