//
//  TOLNavigationController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLNavigationController.h"
#import "TOLSourceTableViewController.h"
#import "TOLLookFeedViewController.h"
#import "TOLFavoritesViewController.h"
#import "TOLInspirationViewController.h"

@interface TOLNavigationController () <UINavigationControllerDelegate>

@property (nonatomic, strong) UIBarButtonItem *searchButton;
@property (nonatomic, strong) UIBarButtonItem *menuButton;
@property (nonatomic, strong) UIViewController *currentViewController;

@end

@implementation TOLNavigationController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (!self) {
        return nil;
    }
    self.delegate = self;
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.delegate = self;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationBar.tintColor = [TOLColor whiteColor];
    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont fontForTitle],
                                      NSForegroundColorAttributeName:[UIColor whiteColor],
                                      NSKernAttributeName:@(-1.75)};
    
    self.navigationBar.titleTextAttributes = titleAttributes;
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated

{
    self.currentViewController = viewController;
//    viewController.navigationItem.rightBarButtonItem = self.searchButton;
    
    if ([self.currentViewController isMemberOfClass:[TOLInspirationViewController class]]) {
        self.navigationBar.barTintColor = [TOLColor lightBlueColor];
    } else if ([self.currentViewController isMemberOfClass:[TOLFavoritesViewController class]]) {
        self.navigationBar.barTintColor = [TOLColor lightYellowColor];
    }
}

#pragma mark -

- (void)searchButtonTapped:(UIBarButtonItem *)buttonTapped
{
    [UIView animateWithDuration:0.1 animations:^{
        self.currentViewController.navigationItem.titleView.alpha = 0;
    }];
    
    TOLSourceTableViewController *sourceViewController = [[TOLSourceTableViewController alloc] init];
    [self pushViewController:sourceViewController animated:YES];
}

#pragma mark - 
- (UIBarButtonItem *)searchButton
{
    if (_searchButton) {
        return _searchButton;
    }
    
    _searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonTapped:)];
    
    return _searchButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
