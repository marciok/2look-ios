//
//  TOLCategoriesNavigationController.m
//  2look
//
//  Created by Marcio Klepacz on 19/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLCategoriesNavigationController.h"

@interface TOLCategoriesNavigationController ()

@end

@implementation TOLCategoriesNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.barTintColor = [TOLColor lightRedColor];
    
}

@end
