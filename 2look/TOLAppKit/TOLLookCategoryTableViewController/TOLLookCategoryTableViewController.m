//
//  TOLLookCategoryTableViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLLookCategoryTableViewController.h"
#import "TOLLookCategoryTableViewCell.h"
#import "TOLLookFeedViewController.h"
#import "LookCategory.h"

static NSString *const kTOLLookCategoryTypeCellIndetifier = @"lookCategoryTypeCellIndentifier";
static CGFloat const kTOLTableCellHeight = 240.0;

@interface TOLLookCategoryTableViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation TOLLookCategoryTableViewController

- (instancetype)initWithLookCategoryType:(TOLLookCategoryType)lookCategoryType
{
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    self.lookCategoryType = lookCategoryType;
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (!self) {
        return nil;
    }
    
    self.lookCategoryType = TOLLookCategoryTypeAll;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"";
    UISegmentedControl *segmentedControl  = [[UISegmentedControl alloc] initWithItems:@[@"Garments", @"Occasions", @"Blogs"]];
    self.navigationItem.titleView = segmentedControl;
    
    [segmentedControl addTarget:self action:@selector(segmentedControlTapped:) forControlEvents:UIControlEventValueChanged];
    segmentedControl.selectedSegmentIndex = 0;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"TOLLookCategoryTableViewCell" bundle:nil] forCellReuseIdentifier:kTOLLookCategoryTypeCellIndetifier];
    self.tableView.backgroundColor = [TOLColor ultraLightGrayColor];
    
    self.fetchedResultsController = [LookCategory fetchAllForType:TOLLookCategoryTypeGarments];
}

- (void)segmentedControlTapped:(UISegmentedControl *)segmentedControl
{
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            self.navigationController.navigationBar.barTintColor = [TOLColor lightRedColor];
            self.fetchedResultsController = [LookCategory fetchAllForType:TOLLookCategoryTypeGarments];
            [self.tableView reloadData];
            break;
        case 1:
            self.navigationController.navigationBar.barTintColor = [TOLColor lightOrangeColor];
            self.fetchedResultsController = [LookCategory fetchAllForType:TOLLookCategoryTypeOccasions];
            [self.tableView reloadData];
            break;
        case 2:
            self.navigationController.navigationBar.barTintColor = [TOLColor lightGreenColor];
            self.fetchedResultsController = [LookCategory fetchAllForType:TOLLookCategoryTypeSupplier];
            [self.tableView reloadData];
            break;
        default:
            break;
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOLLookCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTOLLookCategoryTypeCellIndetifier forIndexPath:indexPath];
    
    [self configureTableCell:cell forIndexPath:indexPath];
    
    return cell;
}

- (void)configureTableCell:(TOLLookCategoryTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    LookCategory *lookCategory = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lookCategory = lookCategory;
    
    UIColor *currentAppSectionColor = self.navigationController.navigationBar.barTintColor;
    cell.lookCategoryImageView.layer.borderColor = [currentAppSectionColor CGColor];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTOLTableCellHeight;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LookCategory *lookCategory = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSURL *lookCategoryURL = [NSURL URLWithString:[NSString stringWithFormat:@"tolookapp://lookcategories/%@", lookCategory.idOnServer]];
    
    [[UIApplication sharedApplication] openURL:lookCategoryURL];
}

@end
