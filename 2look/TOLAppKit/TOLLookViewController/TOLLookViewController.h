//
//  TOLLookViewController.h
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOLAddToFavoritesView.h"
#import "TOLMoreButton.h"

@interface TOLLookViewController : UIViewController <TOLPullToFavoriteDelegate, TOLMoreButtonDelegate>

@property (nonatomic, strong) NSManagedObjectID *lookId;

@end
