//
//  TOLLookViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLLookViewController.h"
#import "Look.h"
#import "User.h"
#import "TOLAddToFavoritesView.h"
#import "LookCategory.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TOLLookAPIHTTPClient.h"
#import "TOLLookView.h"

@interface TOLLookViewController ()

@property (nonatomic, strong) UIView *lookView;
@property (nonatomic, weak) IBOutlet UIImageView *lookImage;
@property (nonatomic, weak) IBOutlet UIView *footerView;
@property (nonatomic, weak) IBOutlet UIImageView *sourceThumb;
@property (nonatomic, weak) IBOutlet UILabel *sourceNameLabel;
@property (nonatomic, weak) IBOutlet UITextView *hashTagsTextView;
@property (nonatomic, weak) IBOutlet TOLMoreButton *moreButton;

@property (nonatomic, strong) TOLAddToFavoritesView *addToFavoritesView;
@property (nonatomic, strong) NSManagedObjectContext *mainContext;
@property (nonatomic, strong) NSURL *supplierURL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewTopSpaceToSuperVIewConstraint;


@end

@implementation TOLLookViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.lookId, @"You need to provide a lookId");
    
    self.view.backgroundColor = [TOLColor ultraLightGrayColor];
    
    Look *look = (Look *)[self.mainContext objectWithID:self.lookId];
    
    LookCategory *supplier = look.supplier;
    self.title = supplier.name;
    
    self.hashTagsTextView.editable = NO;
    self.hashTagsTextView.textContainer.lineFragmentPadding = 0;
    self.hashTagsTextView.textContainerInset = UIEdgeInsetsZero;
    
    self.moreButton.delegate = self;
    
    [look fetchMoreLookCategoriesWithCompletion:^(NSArray *lookCategoriesUpdateNames){
        
        NSMutableAttributedString *hashTagsAttributedStrings = [[NSMutableAttributedString alloc] init];
        
        for (LookCategory *lookCategory in lookCategoriesUpdateNames) {
            NSLog(@"cateogry names %@", lookCategory.name);

            NSString *categoryHashTag = [NSString stringWithFormat:@"#%@ ", lookCategory.name];
            NSString *categoryURLAsAString = [NSString stringWithFormat:@"tolookapp://lookcategories/%@", lookCategory.idOnServer];
            NSURL *categoryURL = [NSURL URLWithString:categoryURLAsAString];
            
            NSDictionary *hashTagAttributes = @{NSLinkAttributeName : categoryURL, NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-light" size:15.0f]};
            NSAttributedString *hashTagWithAttributes = [[NSAttributedString alloc] initWithString:categoryHashTag attributes:hashTagAttributes];
            [hashTagsAttributedStrings appendAttributedString:hashTagWithAttributes];
        }
        
        self.hashTagsTextView.attributedText = hashTagsAttributedStrings;
  }];
    

    [self.lookImage sd_setImageWithURL:[NSURL URLWithString:look.pictureURL]];

    CGFloat starMarkerWidth = 35;
    CGRect starFrame = CGRectMake(CGRectGetMaxX(self.lookImage.frame) - 10 - starMarkerWidth, CGRectGetMinY(self.lookImage.frame) - 3, starMarkerWidth, 20);

    self.addToFavoritesView = [[TOLAddToFavoritesView alloc] initWithFrame:starFrame];
    self.addToFavoritesView.favorited = look.favoritedValue;
    self.addToFavoritesView.pullToFavoriteDelegate = self;
    [self.view addSubview:self.addToFavoritesView];

    [self.sourceThumb sd_setImageWithURL:[NSURL URLWithString:supplier.thumbURL]];
    self.sourceThumb.layer.cornerRadius = CGRectGetHeight(self.sourceThumb.frame) / 2;
    self.sourceThumb.clipsToBounds = YES;

    NSString *sourceFinalTitle = [NSString stringWithFormat:@"@%@", supplier.name];
    self.supplierURL = [NSURL URLWithString:[NSString stringWithFormat:@"tolookapp://lookcategories/%@", supplier.idOnServer]];
    
    NSAttributedString *attributedSourceTitle = [[NSAttributedString alloc] initWithString:sourceFinalTitle attributes:@{NSForegroundColorAttributeName : [TOLColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0f]}];
    
    self.sourceNameLabel.attributedText = attributedSourceTitle;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTitleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.sourceNameLabel addGestureRecognizer:tapGestureRecognizer];
    self.sourceNameLabel.userInteractionEnabled = YES;

    
    //Corner radius
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.footerView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(4.0, 4.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.footerView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.footerView.layer.mask = maskLayer;
    
    [self.view sendSubviewToBack:self.footerView];
    
    self.footerViewTopSpaceToSuperVIewConstraint.constant = -88;
    self.footerView.alpha = 0.0;
    
    [self.view layoutIfNeeded];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
         usingSpringWithDamping:1.5
          initialSpringVelocity:1.5
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                            self.footerViewTopSpaceToSuperVIewConstraint.constant = 0;
                         
                            [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.footerView.alpha = 1.0;
    }];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{

}

//- (BOOL)hidesBottomBarWhenPushed
//{
//    return YES;
//}

- (NSManagedObjectContext *)mainContext
{
    if (_mainContext) {
        return _mainContext;
    }
    
    return _mainContext = [NSManagedObjectContext mainContext];
}

#pragma mark - UITapGestureRecognizer

- (void)handleTitleTap:(UITapGestureRecognizer *)tapRecognizer
{
    [[UIApplication sharedApplication] openURL:self.supplierURL];
}


#pragma mark - TOLPullToFavoriteDelegate

- (void)addToFavorites
{
    Look *lookToFavorite = (Look *)[self.mainContext objectWithID:self.lookId];
    User *currentUser = [User currentUserInContext:self.mainContext];
    
    [currentUser addFavoritesObject:lookToFavorite];
    
    [self.mainContext saveAndWait];    
}

- (void)removeFromFavorites
{
    Look *lookToRemove = (Look *)[self.mainContext objectWithID:self.lookId];
    User *currentUser = [User currentUserInContext:self.mainContext];
    
    [currentUser removeFavoritesObject:lookToRemove];
    
    [self.mainContext saveAndWait];
}

#pragma mark - TOLMoreButtonDelegate

- (void)touchUpInside
{    
    NSString *shareLook = @"Check out this look !";
    UIImage *lookImage = self.lookImage.image;
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[shareLook, lookImage] applicationActivities:nil];
    
    [self.navigationController presentViewController:activityViewController animated:YES completion:nil];
}

@end
