//
//  TOLAppKit.h
//  2look
//
//  Created by Marcio Klepacz on 6/20/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

typedef NS_ENUM(NSUInteger, TOLAppSection) {
    TOLAppSectionFeed,
    TOLAppSectionWho,
    TOLAppSectionWhat,
    TOLAppSectionWhen,
    TOLAppSectionFavorites
};


typedef NS_ENUM(NSUInteger, TOLLookCategoryType) {
    TOLLookCategoryTypeGarments,
    TOLLookCategoryTypeSupplier,
    TOLLookCategoryTypeOccasions,
    TOLLookCategoryTypeAll
};

#import <Foundation/Foundation.h>

