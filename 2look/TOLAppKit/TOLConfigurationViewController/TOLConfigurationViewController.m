//
//  TOLConfigurationViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLConfigurationViewController.h"
#import "TOLConfigurationTableViewController.h"

@interface TOLConfigurationViewController ()

@end

@implementation TOLConfigurationViewController

+ (instancetype)sharedConfigurationsInstance
{
    static TOLConfigurationViewController *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[TOLConfigurationViewController alloc] initWithConfingurations];
    });
    
    return _sharedInstance;
}

- (instancetype)initWithConfingurations
{
    TOLConfigurationTableViewController *configurationTableViewController = [[TOLConfigurationTableViewController alloc] init];
    
    self = [super initWithRootViewController:configurationTableViewController];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationBar.barTintColor = [TOLColor lightGrayColor];
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [TOLColor darkGrayColor], NSFontAttributeName :  [UIFont fontForSubtitle]};
    self.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
