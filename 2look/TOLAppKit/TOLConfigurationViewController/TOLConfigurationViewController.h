//
//  TOLConfigurationViewController.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLConfigurationViewController : UINavigationController

+ (instancetype)sharedConfigurationsInstance;

@end
