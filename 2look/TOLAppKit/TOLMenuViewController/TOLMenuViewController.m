//
//  TOLMenuViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLMenuViewController.h"
#import "TOLConfigurationViewController.h"
#import "TOLMenuTableView.h"
#import "TOLMenuTableViewCell.h"
#import "TOLButton.h"
#import "TOLNavigationController.h"
#import "TOLLookCategoryTableViewController.h"

static CGFloat const kTOLNavigationbarHeight = 64.0;
static CGFloat const kTOLMenuFooterHeight = 57.0;
static CGFloat const kTOLMenuTableViewCellHeight = 50.0;
static CGFloat const kTOLleftPanelWitdh = 256.0;
static NSUInteger const kTOLNumberOfMenuTableRows = 5;
static NSUInteger const kTOLNumberOfMenuSections = 1;
static NSString *const kTOLMenuCellIndetifier = @"TOLMenuTableViewCellIdentifier";

@interface TOLMenuViewController ()

@property (nonatomic, strong) TOLMenuTableView *menuTableView;
@property (nonatomic, strong) UIView *menuFooterView;

@end

@implementation TOLMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [TOLColor lightGrayColor];
    
    [self setupMenuTableView];
    
    [self menuFooterView];
}

- (void)setupMenuTableView
{
    self.menuTableView = [[TOLMenuTableView alloc] init];
    self.menuTableView.frame = CGRectMake(self.view.frame.origin.x,
                                          kTOLNavigationbarHeight,
                                          CGRectGetWidth(self.view.frame),
                                          CGRectGetHeight(self.view.frame) - kTOLNavigationbarHeight - kTOLNavigationbarHeight);
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    [self.menuTableView registerClass:[TOLMenuTableViewCell class] forCellReuseIdentifier:kTOLMenuCellIndetifier];
    
    [self.view addSubview:self.menuTableView];
}

- (void)setupMenuFooterView
{    CGFloat menuVisibleWidth = [self leftPanelWitdh];
    
    self.menuFooterView = [[UIView alloc] init];
    self.menuFooterView.frame = CGRectMake(self.view.frame.origin.x, CGRectGetHeight(self.menuTableView.frame) + kTOLNavigationbarHeight, menuVisibleWidth, kTOLMenuFooterHeight);
    
    [self setupConfigurationButton];
    
    [self.view addSubview:self.menuFooterView];
}

- (void)setupConfigurationButton
{
    UIButton *configurationButton = [TOLButton buttonWithConfigurationType];
    
    [self.menuFooterView addSubview:configurationButton];
    [configurationButton sizeToFit];
    configurationButton.center = CGPointMake(self.menuFooterView.frame.size.width / 2, self.menuFooterView.frame.size.height / 2);
    
    [configurationButton addTarget:self action:@selector(configurationTaped) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat middleYfooter = CGRectGetHeight(self.menuFooterView.frame);
    CGFloat middleXfooter = CGRectGetMidX(self.menuFooterView.frame);
    configurationButton.center = CGPointMake(middleXfooter, middleYfooter / 2);
}

- (void)configurationTaped
{
    TOLConfigurationViewController *configurationViewController = [TOLConfigurationViewController sharedConfigurationsInstance];
    
    [self presentViewController:configurationViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return kTOLNumberOfMenuSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return kTOLNumberOfMenuTableRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TOLMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTOLMenuCellIndetifier forIndexPath:indexPath];
    
    //    [self animateBackgroundFromCell:cell];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(TOLMenuTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    TOLAppSection currentSection = indexPath.row;
    NSDictionary *appSectionAttributes = [TOLHelpers attributesForAppSection:currentSection];
    
    cell.menuCellTitle.text = appSectionAttributes[@"sectionTitle"];
    cell.menuCellBackgroundView.backgroundColor = appSectionAttributes[@"sectionBackgroundColor"];
    cell.menuCellTitle.textColor = appSectionAttributes[@"sectionMenuTitleColor"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTOLMenuTableViewCellHeight;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOLMenuTableViewCell *cell = (TOLMenuTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TOLAppSection selectedSection = indexPath.row;
    
    NSDictionary *sectionAttributes = [TOLHelpers attributesForAppSection:selectedSection];    
    
    [[UIApplication sharedApplication] openURL:sectionAttributes[@"sectionRoute"]];
    
    [self.sidePanelController showCenterPanelAnimated:YES];

}

//Supended
//-(void) animateBackgroundFromCell:(TOLMenuTableViewCell *)cell
//{
//    float cellWidth = cell.menuItemBackgroundView.frame.size.width;
//    NSUInteger cellNewWidth = arc4random_uniform(cellWidth - 50) + 150;
//
//    [UIView animateWithDuration:0.7 animations:^{
//        cell.menuItemBackgroundView.frame = CGRectMake(cell.menuItemBackgroundView.frame.origin.x,
//                                                       cell.menuItemBackgroundView.frame.origin.y,
//                                                       cellNewWidth,
//                                                       cell.menuItemBackgroundView.frame.size.height);
//
//    } completion:nil];
//}

@end
