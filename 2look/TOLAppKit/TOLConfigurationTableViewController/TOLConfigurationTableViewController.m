//
//  TOLConfigurationTableViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLConfigurationTableViewController.h"

static NSString *const kconfigurationCellIdentifier = @"ConfCell";

typedef NS_ENUM(NSUInteger, ConfigrationsSections){
    ConfigurationsSectionPreferences,
    ConfigurationsSectionFeedBack,
    ConfigurationsSectionSupport,
};

@interface TOLConfigurationTableViewController ()

@property (nonatomic, strong) UIBarButtonItem *dismissButton;
@property (nonatomic, strong) NSArray *sectionTitles;

@end

@implementation TOLConfigurationTableViewController

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.dismissButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissButtonTap)];
    
    return self;
}

- (void)dismissButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Configurations";
    self.navigationItem.leftBarButtonItem = self.dismissButton;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kconfigurationCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    self.sectionTitles = @[@"Preferences", @"Feedback", @"Help"];
    // Return the number of sections.
    
    return [self.sectionTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    switch (section) {
        case ConfigurationsSectionPreferences:
            numberOfRows = 3;
            break;
        case ConfigurationsSectionFeedBack:
            numberOfRows = 3;
            break;
        case ConfigurationsSectionSupport:
            numberOfRows = 3;
            break;
    }
    
    // Return the number of rows in the section.
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kconfigurationCellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.textLabel.text = @"Rate-me";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.sectionTitles[section];
}

@end
