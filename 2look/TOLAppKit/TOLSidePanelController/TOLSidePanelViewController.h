//
//  TOLSidePanelViewController.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "JASidePanelController.h"

@protocol LeftPanelSizeProtocol <NSObject>

@optional

- (CGFloat)leftPanelWitdh;

@end

@interface TOLSidePanelViewController : JASidePanelController

@end


