//
//  TOLSidePanelViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLSidePanelViewController.h"

@interface TOLSidePanelViewController ()

@end

@implementation TOLSidePanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.leftFixedWidth = [self.leftPanelSizeDelegate leftPanelWitdh];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)stylePanel:(UIView *)panel
{
    panel.layer.cornerRadius = 0.0f;
    panel.clipsToBounds = NO;
    
}

- (void)styleContainer:(UIView *)container animate:(BOOL)animate duration:(NSTimeInterval)duration
{
    container.layer.shadowColor = [UIColor clearColor].CGColor;
    container.layer.shadowRadius = 0.0f;
    container.layer.shadowOpacity = 0.0f;
    container.clipsToBounds = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
