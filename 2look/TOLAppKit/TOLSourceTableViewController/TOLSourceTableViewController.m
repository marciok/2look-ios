//
//  TOLSourceTableViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLSourceTableViewController.h"
#import "TOLSourceTableViewCell.h"
#import "TOLLookFeedViewController.h"
#import "LookCategory.h"
#import "TOLNavigationController.h"

static NSString * const kTOLSourceCellIndentifier = @"sourceCellIndentifier";
static CGFloat const kTOLSearchTableCellHeight = 105.0;

@interface TOLSourceTableViewController ()

@end

@implementation TOLSourceTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = [TOLColor ultraLightGrayColor];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TOLSourceTableViewCell" bundle:nil] forCellReuseIdentifier:kTOLSourceCellIndentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.searchFetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.searchFetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOLSourceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTOLSourceCellIndentifier forIndexPath:indexPath];
    
    //TODO: This is DUMB refactor that ASAP
    LookCategory *lookCategory = [self.searchFetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lookCategoryId = [lookCategory objectID];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTOLSearchTableCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOLSourceTableViewCell *cell = (TOLSourceTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    //TODO: Create a singleton for that ?
    TOLLookFeedViewController *lookFeedViewController = [[TOLLookFeedViewController alloc] initWithLookCategoryId:cell.lookCategoryId];
    
    lookFeedViewController.lookCategoryId = cell.lookCategoryId;
    
    UITabBarController *tabBar = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    TOLNavigationController *currentNavigationController = (TOLNavigationController *)tabBar.selectedViewController;
    
    
    [currentNavigationController pushViewController:lookFeedViewController animated:YES];
}

@end
