//
//  TOLSourceTableViewController.h
//  2look
//
//  Created by Marcio Klepacz on 5/24/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLSourceTableViewController : UITableViewController

@property (nonatomic, strong) NSFetchedResultsController *searchFetchedResultsController;

@end
