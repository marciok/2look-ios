//
//  TOLHelpers.h
//  2look
//
//  Created by Marcio Klepacz on 6/20/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TOLHelpers : NSObject

+ (NSString *)nameOnTOLAPITForLookCategoryType:(TOLLookCategoryType)lookCategoryType;

+ (NSDictionary *)attributesForAppSection:(TOLAppSection)appSection;

+ (TOLAppSection)appSectionForSectionName:(NSString *)appSectionName;

+ (Class)viewControllerClassForAppSectionName:(NSString *)appSectionName;

+ (TOLLookCategoryType)lookCategoryTypeForAppSectionName:(NSString *)appSectionName;

+ (UIColor *)randomThemeColor;

@end

