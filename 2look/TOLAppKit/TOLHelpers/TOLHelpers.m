//
//  TOLHelpers.m
//  2look
//
//  Created by Marcio Klepacz on 6/20/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "NSString+TOLStringDetection.h"
#import "TOLLookCategoryTableViewController.h"
#import "TOLLookFeedViewController.h"

static NSUInteger themeColorIndex = 0;

@implementation TOLHelpers

+ (NSString *)nameOnTOLAPITForLookCategoryType:(TOLLookCategoryType)lookCategoryType
{
    NSString *typeName;
    
    switch (lookCategoryType) {
        case TOLLookCategoryTypeGarments:
            typeName = @"garments";
            break;
        case TOLLookCategoryTypeOccasions:
            typeName = @"occasions";
            break;
        case TOLLookCategoryTypeAll:
        case TOLLookCategoryTypeSupplier:
            typeName = @"sources"; 
            break;
    }
    
    return typeName;
}

+ (TOLAppSection)appSectionForSectionName:(NSString *)appSectionName
{
    TOLAppSection appSection;
    
    if ([appSectionName isEqualToString:@"feed"]) {
        appSection = TOLAppSectionFeed;
        
    } else if ([appSectionName isEqualToString:@"suppliers"]){
        appSection = TOLAppSectionWho;
        
    } else if ([appSectionName isEqualToString:@"garments"]){
        appSection = TOLAppSectionWhat;
        
    } else if ([appSectionName isEqualToString:@"occasions"]){
        appSection = TOLAppSectionWhen;
        
    } else if ([appSectionName isEqualToString:@"favorites"]){
        appSection = TOLAppSectionFavorites;
    }
    
    return appSection;
}

+ (NSDictionary *)attributesForAppSection:(TOLAppSection)appSection
{
    NSString *sectionTitle;
    UIColor *sectionBackgroundColor;
    UIColor *sectionMenuTitleColor;
    NSURL *sectionRoute;
    
    switch (appSection) {
        case TOLAppSectionFeed:
            sectionTitle = @"Feed";
            sectionBackgroundColor = [TOLColor lightBlueColor];
            sectionMenuTitleColor = [TOLColor darkBlueColor];
            sectionRoute = [NSURL URLWithString:@"tolookapp://feed/"];
            break;
        case TOLAppSectionWho:
            sectionTitle = @"Who?";
            sectionBackgroundColor = [TOLColor lightGreenColor];
            sectionMenuTitleColor = [TOLColor darkGreenColor];
            sectionRoute = [NSURL URLWithString:@"tolookapp://suppliers/"];
            break;
        case TOLAppSectionWhat:
            sectionTitle = @"What?";
            sectionBackgroundColor = [TOLColor lightRedColor];
            sectionMenuTitleColor = [TOLColor darkRedColor];
            sectionRoute = [NSURL URLWithString:@"tolookapp://garments/"];
            break;
        case TOLAppSectionWhen:
            sectionTitle = @"When?";
            sectionBackgroundColor = [TOLColor lightOrangeColor];
            sectionMenuTitleColor = [TOLColor darkOrangeColor];
            sectionRoute = [NSURL URLWithString:@"tolookapp://occasions/"];
            break;
        case TOLAppSectionFavorites:
            sectionTitle = @"Favorites";
            sectionBackgroundColor = [TOLColor lightYellowColor];
            sectionMenuTitleColor = [TOLColor darkYellowColor];
            sectionRoute = [NSURL URLWithString:@"tolookapp://favorites/"];
            break;
        }
    
    return @{@"sectionTitle" : sectionTitle,
             @"sectionBackgroundColor" : sectionBackgroundColor,
             @"sectionMenuTitleColor" : sectionMenuTitleColor,
             @"sectionRoute" : sectionRoute};
}

+ (Class)viewControllerClassForAppSectionName:(NSString *)appSectionName
{
    Class viewControllerClass;
    
    if ([appSectionName isSectionWithLookCategory]) {
        viewControllerClass =  [TOLLookCategoryTableViewController class];
    } else if ([appSectionName isSectionWithFeed]) {
        viewControllerClass = [TOLLookFeedViewController class];
    }
    
    return viewControllerClass;
}

+ (TOLLookCategoryType)lookCategoryTypeForAppSectionName:(NSString *)appSectionName
{
    TOLLookCategoryType lookCategoryType;
    
    if ([appSectionName isEqualToString:@"garments"]) {
        lookCategoryType = TOLLookCategoryTypeGarments;
    } else if ([appSectionName isEqualToString:@"occasions"]){
        lookCategoryType = TOLLookCategoryTypeOccasions;
    } else if ([appSectionName isEqualToString:@"suppliers"]){
        lookCategoryType = TOLLookCategoryTypeSupplier;
    }
    
    return lookCategoryType;
}

+ (UIColor *)randomThemeColor
{
    NSArray *colors = @[[TOLColor lightRedColorHalfAlpha], [TOLColor lightBlueColorHalfAlpha], [TOLColor lightYellowColorHalfAlpha], [TOLColor lightGreenColorHalfAlpha], [TOLColor lightOrangeColorHalfAlpha]];
    
    
    if ([colors count] < themeColorIndex + 1) {
        themeColorIndex = 0;
    }
    
    return colors[themeColorIndex++];
}

@end
