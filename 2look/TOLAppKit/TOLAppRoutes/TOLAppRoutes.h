//
//  TOLAppRoutes.h
//  2look
//
//  Created by Marcio Klepacz on 6/27/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "JLRoutes.h"

@interface TOLAppRoutes : NSObject

+ (void)setupRoutes;

@end
