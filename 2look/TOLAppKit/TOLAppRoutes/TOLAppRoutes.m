//
//  TOLAppRoutes.m
//  2look
//
//  Created by Marcio Klepacz on 6/27/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLAppRoutes.h"
#import "TOLAppDelegate.h"
#import "TOLNavigationController.h"
#import "NSString+TOLStringDetection.h"
#import "TOLLookCategoryTableViewController.h"
#import "TOLLookFeedViewController.h"
#import "LookCategory.h"

@implementation TOLAppRoutes

#pragma mark - Attr

+ (TOLNavigationController *)feedNavigationController
{
    TOLAppDelegate *appDelegate = (TOLAppDelegate *)[UIApplication sharedApplication].delegate;
    
    return appDelegate.navigationController;
}

+ (void)setupRoutes
{
    JLRoutes *routes = [JLRoutes routesForScheme:@"tolookapp"];

#ifdef DEBUG
    [JLRoutes setVerboseLoggingEnabled:YES];
#endif
    
    [routes addRoute:@"/:app_section/" handler:^BOOL(NSDictionary *parameters) {
        
        NSString *appSectionName = parameters[@"app_section"];
        
        TOLAppSection appSection = [TOLHelpers appSectionForSectionName:appSectionName];
        NSDictionary *appSectionAttributes = [TOLHelpers attributesForAppSection:appSection];
        
        //TODO: Change to accept section NOT section name
        Class viewControllerClass = [TOLHelpers viewControllerClassForAppSectionName:appSectionName];
        
        id viewController;
        
        if ([appSectionName isSectionWithLookCategory]) {
            TOLLookCategoryType lookCategoryType = [TOLHelpers lookCategoryTypeForAppSectionName:appSectionName];
            viewController = [[TOLLookCategoryTableViewController alloc] initWithLookCategoryType:lookCategoryType];
        }
        else {
            viewController = [[viewControllerClass alloc] init];
        }
        
        NSString *viewControllerTitle = appSectionAttributes[@"sectionTitle"];
        
        [viewController setTitle:viewControllerTitle];
        
        UIColor *navigationBarColor = appSectionAttributes[@"sectionBackgroundColor"];
        
        [self.feedNavigationController.navigationBar setBarTintColor:navigationBarColor];
        self.feedNavigationController.viewControllers = @[viewController];
        
        return YES;
    }];
    
    [routes addRoute:@"/lookcategories/:id_on_server" handler:^BOOL(NSDictionary *parameters) {
        
        NSExpressionDescription* objectIdDesc = [[NSExpressionDescription alloc] init];
        objectIdDesc.name = @"objectID";
        objectIdDesc.expression = [NSExpression expressionForEvaluatedObject];
        objectIdDesc.expressionResultType = NSObjectIDAttributeType;
        
        NSPredicate *findByServerId = [NSPredicate predicateWithFormat:@"idOnServer == %@", parameters[@"id_on_server"]];
        LookCategory *lookCategory = [LookCategory MR_findFirstWithPredicate:findByServerId andRetrieveAttributes:@[objectIdDesc]];
        
        if (!lookCategory) {
            return NO;
        }
        
        TOLLookFeedViewController *lookFeedViewController = [[TOLLookFeedViewController alloc] initWithLookCategoryId:lookCategory.objectID];
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        UITabBarController *rootTabBarController = (UITabBarController *)window.rootViewController;
        
        UINavigationController *navigationController = (UINavigationController *)rootTabBarController.selectedViewController;
        
        [navigationController pushViewController:lookFeedViewController animated:YES];
        
        return YES;
    }];
}


@end
