//
//  TOLTabBarViewController.m
//  2look
//
//  Created by Marcio Klepacz on 19/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLTabBarViewController.h"

@interface TOLTabBarViewController ()


@end

@implementation TOLTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITabBarItem *inspirationItem = [self.tabBar.items firstObject];
    inspirationItem.title = @"Inspiration";
    
    UITabBarItem *categoriesItem = self.tabBar.items[1];
    categoriesItem.title = @"Categories";
    
    
    UITabBarItem *favoritesIntem = self.tabBar.items[2];
    favoritesIntem.title = @"Favorites";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
