//
//  TOLDropDownMenuAnimator.m
//  2look
//
//  Created by Marcio Klepacz on 11/01/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLDropDownMenuAnimator.h"

@implementation TOLDropDownMenuAnimator

- (instancetype)initWithNavigationController:(UINavigationController *)nav toViewController:(UIViewController *)vc
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.viewController = @{UITransitionContextFromViewControllerKey:nav, UITransitionContextToViewControllerKey:vc};
    
    return self;
}


#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.3;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
}
- (void)animationEnded:(BOOL)transitionCompleted
{
    
}


@end
