//
//  TOLDropDownMenuViewController.m
//  2look
//
//  Created by Marcio Klepacz on 11/01/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLDropDownMenuViewController.h"
#import "TOLAppDelegate.h"
#import "TOLNavigationController.h"

@interface TOLDropDownMenuViewController ()

@end

@implementation TOLDropDownMenuViewController

+ (instancetype)sharedInstance
{
    static TOLDropDownMenuViewController *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *screenShotImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    
    UIGraphicsEndImageContext();
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [[CIImage alloc] initWithImage:self.screenShot];
    CIFilter *blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setDefaults];
    [blurFilter setValue:inputImage forKey:@"inputImage"];
    CGFloat blurLevel = 3.0f;          // Set blur level
    [blurFilter setValue:[NSNumber numberWithFloat:blurLevel] forKey:@"inputRadius"];    // set value for blur level
    CIImage *outputImage = [blurFilter valueForKey:@"outputImage"];
    CGRect rect = inputImage.extent;    // Create Rect
    rect.origin.x += blurLevel;         // and set custom params
    rect.origin.y += blurLevel;         //
    rect.size.height -= blurLevel*2.0f; //
    rect.size.width -= blurLevel*2.0f;  //
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:rect];    // Then apply new rect
    screenShotImageView.image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    
//    screenShotImageView.image = self.screenShot;
//    screenShotImageView.backgroundColor = [UIColor greenColor]
//
//    [screenShotImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:screenShotImageView];
//
//    NSDictionary *metricDict = @{@"w": @(self.view.bounds.size.width), @"h":@(self.view.bounds.size.height)};
//    
//    [screenShotImageView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(w)]" options:0 metrics:metricDict views:@{@"view": screenShotImageView}]];
//    
//    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[screenShotImageView]" options:0 metrics:nil views:@{@"screenShotImageView" : screenShotImageView}];
//    
//    
//    [self.view addConstraints:constraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
