//
//  TOLDropDownMenuAnimator.h
//  2look
//
//  Created by Marcio Klepacz on 11/01/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TOLDropDownMenuAnimator : NSObject <UIViewControllerAnimatedTransitioning>

- (instancetype)initWithNavigationController:(UINavigationController *)nav toViewController:(UIViewController *)vc;

@property (nonatomic, strong) NSDictionary *viewController;

@end
