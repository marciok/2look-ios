//
//  TOLDropDownMenuViewController.h
//  2look
//
//  Created by Marcio Klepacz on 11/01/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOLDropDownMenuViewController : UIViewController <UIViewControllerAnimatedTransitioning>

@property (nonatomic, strong) UIImage *screenShot;

+ (instancetype)sharedInstance;

@end
