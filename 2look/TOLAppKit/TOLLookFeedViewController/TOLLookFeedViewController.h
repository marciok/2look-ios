//
//  TOLLookViewController.h
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>

@interface TOLLookFeedViewController : UIViewController <UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout>

- (instancetype)initWithLookCategoryId:(NSManagedObjectID *)lookCategoryId;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSManagedObjectID *lookCategoryId;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end
