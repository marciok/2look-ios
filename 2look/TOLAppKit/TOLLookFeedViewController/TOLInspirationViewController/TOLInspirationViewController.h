//
//  TOLInspirationViewController.h
//  2look
//
//  Created by Marcio Klepacz on 19/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLLookFeedViewController.h"

@interface TOLInspirationViewController : TOLLookFeedViewController <UISearchBarDelegate, UISearchResultsUpdating>

@end
