//
//  TOLInspirationViewController.m
//  2look
//
//  Created by Marcio Klepacz on 19/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLInspirationViewController.h"
#import "TOLSourceTableViewController.h"
#import "TOLNavigationController.h"
#import "LookCategory.h"

@interface TOLInspirationViewController ()

@property (nonatomic, strong) NSFetchedResultsController *searchFetchedResultsController;
//@property (nonatomic, strong) UISearchDisplayController *searchALookCategoryDisplayController;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableView *searchResultsTableView;

@end

@implementation TOLInspirationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Inspiration";
    
    TOLSourceTableViewController *sourcesViewController = [[TOLSourceTableViewController alloc] init];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:sourcesViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.placeholder = @"Get Inspired";

    self.navigationItem.titleView = self.searchController.searchBar;

    self.definesPresentationContext = YES;

}

#pragma mark - UISearchDisplayDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSPredicate *allLookCategoriesPredicate = [NSPredicate predicateWithFormat: @"name CONTAINS[c] %@", searchController.searchBar.text];
    
    self.searchFetchedResultsController = [LookCategory MR_fetchAllGroupedBy:nil withPredicate:allLookCategoriesPredicate sortedBy:nil ascending:YES inContext:[NSManagedObjectContext mainContext]];
    TOLSourceTableViewController *sourceViewController = (TOLSourceTableViewController *)searchController.searchResultsController;
    sourceViewController.searchFetchedResultsController = self.searchFetchedResultsController;
    
    [sourceViewController.tableView reloadData];
}

@end
