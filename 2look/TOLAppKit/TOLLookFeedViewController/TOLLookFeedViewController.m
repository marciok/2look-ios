//
//  TOLLookFeedViewController.m
//  2look
//
//  Created by Marcio Klepacz on 5/23/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLLookFeedViewController.h"
#import "TOLLookCollectionViewCell.h"
#import "TOLLookViewController.h"
#import "TOLLookFeedFetchedResultsControllerDelegate.h"
#import "Look.h"
#import "User.h"
#import "LookCategory.h"
#import "TOLMenuBarButtonItem.h"
#import "TOLNavigationController.h"

//TODO Those should be in the same file
#import "TOLDropDownMenuViewController.h"
#import "TOLDropDownMenuAnimator.h"
#import <CoreImage/CoreImage.h>

typedef NS_ENUM(NSUInteger, TOLLookCellPosition) {
    TOLLookCellPositionLeftTop,
    TOLLookCellPositionRightTop,
    TOLLookCellPositionLeftBottom,
    TOLLookCellPositionRightBottom
};

static CGFloat const kTOLookCellWidth = 308;
static NSString *const kTOLLookFeedCellIndentifier = @"LookCellIndentifier";

@interface TOLLookFeedViewController () <TOLLookCollectionViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *cellSizes;
@property (nonatomic, strong) TOLLookFeedFetchedResultsControllerDelegate *lookFeedFetchedResultsControllerDelegate;
@property (nonatomic, assign) NSInteger cellCount;
@property (nonatomic, assign) BOOL isloadingLooks;
@property (nonatomic, strong) NSString *lastLookServerId;

@end

@implementation TOLLookFeedViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{   self = [super self];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (instancetype)initWithLookCategoryId:(NSManagedObjectID *)lookCategoryId
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.lookCategoryId = lookCategoryId;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isloadingLooks = NO;
    
    [self.view addSubview:self.collectionView];
    
    NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
    
    if (self.lookCategoryId) {
        LookCategory *currentLookCategory = (LookCategory *)[mainContext objectWithID:self.lookCategoryId];
        self.title = currentLookCategory.name;
        
        self.fetchedResultsController = [Look fetchLooksForCategoryId:self.lookCategoryId inContext:mainContext];
        self.fetchedResultsController.delegate = self.lookFeedFetchedResultsControllerDelegate;
        
        if ([self.fetchedResultsController.fetchedObjects count] < 7) {
            [Look addLatestLooksFromTOLAPIInContext:mainContext savingEach:NO withMaxIdOnAPI:nil forLookCategoryId:_lookCategoryId withCompletion:nil];
        }
    }
    else if ([self.title isEqualToString:@"Favorites"]){
        User *currentUser = [User currentUserInContext:mainContext];
        self.fetchedResultsController = [currentUser fetchFavoritesInContext:mainContext];
    }
    else  {
        self.fetchedResultsController = [Look fetchLooksInContext:mainContext];
        self.fetchedResultsController.delegate = self.lookFeedFetchedResultsControllerDelegate;
    }
}

- (TOLLookFeedFetchedResultsControllerDelegate *)lookFeedFetchedResultsControllerDelegate
{
    if (_lookFeedFetchedResultsControllerDelegate) {
        return _lookFeedFetchedResultsControllerDelegate;
    }
    
    _lookFeedFetchedResultsControllerDelegate = [[TOLLookFeedFetchedResultsControllerDelegate alloc] initWithCollectionView:self.collectionView];
    
    return _lookFeedFetchedResultsControllerDelegate;
}

- (UICollectionView *)collectionView
{
    if (_collectionView) {
        return _collectionView;
    }
    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(5, 10, 8, 10);
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 7;
    layout.minimumInteritemSpacing = 7;
    layout.columnCount = 2;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.backgroundColor = [TOLColor ultraLightGrayColor];
    
    UINib *lookCellNib = [UINib nibWithNibName:@"TOLLookCollectionViewCell" bundle:nil];
    [_collectionView registerNib:lookCellNib forCellWithReuseIdentifier:kTOLLookFeedCellIndentifier];
    
    return _collectionView;
}

- (NSString *)lastLookServerId
{
    Look *lastLooks = [self.fetchedResultsController.fetchedObjects lastObject];
    
    return lastLooks.idOnServer;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        
        NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
        
        [Look addLatestLooksFromTOLAPIInContext:mainContext savingEach:YES withMaxIdOnAPI:self.lastLookServerId forLookCategoryId:self.lookCategoryId withCompletion:nil];
        
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Look *look = [self.fetchedResultsController objectAtIndexPath:indexPath];

    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    TOLNavigationController *currentNavigationController = (TOLNavigationController *)tabBarController.selectedViewController;
    
    TOLLookViewController *lookViewController = [[TOLLookViewController alloc] initWithNibName:@"TOLLookView" bundle:nil];
    lookViewController.hidesBottomBarWhenPushed = YES;
    lookViewController.lookId = [look objectID];
    
    TOLLookCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTOLLookFeedCellIndentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    UIImageView *imageClone = cell.lookImageView;
    
    imageClone.frame = [self.collectionView convertRect:cell.frame toView:self.view];
    imageClone.frame = CGRectMake(imageClone.frame.origin.x, imageClone.frame.origin.y, imageClone.frame.size.width, imageClone.frame.size.height - 36);
    
    UIView *helperView = [[UIView alloc] initWithFrame:self.view.bounds];
    helperView.backgroundColor = self.collectionView.backgroundColor;
    
    UIGraphicsBeginImageContext(self.view.bounds.size);
    
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *snapShotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imageViewSnap = [[UIImageView alloc] initWithImage:snapShotImage];
    helperView.frame = CGRectMake(0, self.collectionView.contentOffset.y, self.view.bounds.size.width, self.view.bounds.size.height);
    
    [self.collectionView addSubview:helperView];
    [helperView addSubview:imageViewSnap];
    
    [helperView addSubview:imageClone];
    
    CGFloat topMargin = self.navigationController ? 73 : 12;
    
    NSArray *constraints1 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[imageClone]-12-|" options:0 metrics:nil views:@{@"imageClone" : imageClone}];
    
    NSArray *constraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topMargin-[imageClone]" options:0 metrics:@{@"topMargin" : @(topMargin)} views:@{@"imageClone" : imageClone}];
   
    NSArray *constraints3 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageClone(395)]" options:0 metrics:nil views:@{@"imageClone" : imageClone}];
   
    [imageClone setTranslatesAutoresizingMaskIntoConstraints:NO];
    [helperView addConstraints:constraints1];
    [helperView addConstraints:constraints2];
    [helperView addConstraints:constraints3];
    
   
    [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        UIView *titleView = self.navigationItem.titleView;
        titleView.alpha = 0.0;
        imageViewSnap.alpha = 0.0;
        tabBarController.tabBar.alpha = 0.0;

        
        [helperView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [currentNavigationController pushViewController:lookViewController animated:NO];
        
        [imageClone performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.5];
        [helperView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.5];
        tabBarController.tabBar.alpha = 1;
        
    }];
    
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize cellSize;
    
    switch (self.cellCount) {
        case TOLLookCellPositionLeftTop:
            self.cellCount++;
            cellSize = CGSizeMake(kTOLookCellWidth, 474);
            break;
        case TOLLookCellPositionRightTop:
            self.cellCount++;
            cellSize = CGSizeMake(kTOLookCellWidth, 534);
            break;
        case TOLLookCellPositionLeftBottom:
            self.cellCount++;
            cellSize = CGSizeMake(kTOLookCellWidth, 554);
            break;
        case TOLLookCellPositionRightBottom:
            self.cellCount = 0;
            cellSize = CGSizeMake(kTOLookCellWidth, 494);
            break;
    }
    
    return cellSize;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id sectionInfo = self.fetchedResultsController.sections[section];
    
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOLLookCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTOLLookFeedCellIndentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(TOLLookCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Look *look = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.lookId = [look objectID];
    
//    cell.footerView.frame = CGRectMake(0, CGRectGetMaxY(cell.bounds) - 36 , CGRectGetWidth(cell.bounds), 36);
    
    //Corner radius
    UIBezierPath *maskPath;
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    if (cell.layer.mask != maskLayer) {
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(4.0, 4.0)];
        
        maskLayer.frame = cell.bounds;
        maskLayer.path = maskPath.CGPath;
        cell.layer.mask = maskLayer;
    }
    
//    cell.lookSourceThumb.frame = CGRectMake(2, 2, 30, 30);
    cell.lookSourceThumb.layer.cornerRadius = CGRectGetHeight(cell.lookSourceThumb.frame) / 2;
    
//    cell.lookSourceNameButton.frame = CGRectMake(CGRectGetMaxX(cell.lookSourceThumb.bounds), CGRectGetMidY(cell.footerView.bounds) - 4,CGRectGetWidth(cell.bounds) - CGRectGetMaxX(cell.lookSourceThumb.bounds) , 9);
}


#pragma mark -
- (void)sizeWithImageLoaded:(CGSize)size
{
}

@end
