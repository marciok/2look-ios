//
//  TOLLookFeedFetchedResultsControllerDelegate.h
//  2look
//
//  Created by Marcio Klepacz on 8/10/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TOLLookFeedFetchedResultsControllerDelegate : NSObject <NSFetchedResultsControllerDelegate>

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

@end
