//
//  TOLFavoritesViewController.m
//  2look
//
//  Created by Marcio Klepacz on 06/04/15.
//  Copyright (c) 2015 Marcio Klepacz. All rights reserved.
//

#import "TOLFavoritesViewController.h"

@interface TOLFavoritesViewController ()

@end

@implementation TOLFavoritesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Favorites";
}


@end
