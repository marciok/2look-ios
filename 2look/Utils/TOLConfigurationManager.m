//
//  TOLConfigurationManager.m
//  2look
//
//  Created by Marcio Klepacz on 5/30/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import "TOLConfigurationManager.h"
#import "TOLLookAPIHTTPClient.h"
#import "Look.h"
#import "User.h"
#import "LookCategory.h"

@implementation TOLConfigurationManager

//TODO: Why a singleton ? and a class method
+ (instancetype)sharedInstance
{
    static TOLConfigurationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)setupCoreData
{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"2lookDataModel"];
    
    BOOL isFirstInstall = ![[NSUserDefaults standardUserDefaults] boolForKey:@"is2lookInstalled"];
    
    if (!isFirstInstall) {
        return;
    }
    /*
     1) Seed looks;
     2) '   ' garments;
     2) '   ' occasions;
     2) '   ' supplier;
     */
    
    // 1) Seeding Looks
    NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
    
    [Look addLatestLooksFromTOLAPIInContext:mainContext savingEach:NO withCompletion:nil];
    
    // 2) Seeding Garments
    [[TOLLookAPIHTTPClient v1SharedClient] allCategoriesForType:TOLLookCategoryTypeGarments withCompletion:^(NSArray *lookCategories, NSError *error){
        
        if (error) {
            return;
        }
        
        NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
        
        TOLLookCategoryType garmentCategoryType = TOLLookCategoryTypeGarments;
        
        [lookCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSDictionary *parameters = @{@"idOnServer" : obj[@"id"],
                                         @"name" : obj[@"name"],
                                         @"thumbURL" : obj[@"thumb"],
                                         @"entries" : obj[@"entries"],
                                         @"type" : @(garmentCategoryType)};
            
            [LookCategory createOrUpdateLookCategoryWithParameters:parameters inContext:mainContext];
        }];
    }];
    
    // 2) Seeding Occasions
    [[TOLLookAPIHTTPClient v1SharedClient] allCategoriesForType:TOLLookCategoryTypeOccasions withCompletion:^(NSArray *lookCategories, NSError *error){
        
        if (error) {
            return;
        }
        
        NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
        
        TOLLookCategoryType occasionsType = TOLLookCategoryTypeOccasions;
        
        [lookCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSDictionary *parameters = @{@"idOnServer" : obj[@"id"],
                                         @"name" : obj[@"name"],
                                         @"thumbURL" : obj[@"thumb"],
                                         @"entries" : obj[@"entries"],
                                         @"type" : @(occasionsType)};
            
            [LookCategory createCategoryWithParameters:parameters inContext:mainContext];
            
        }];
        
        [mainContext saveAndWait];
    }];
    
    // 3) Seeding Suppliers
   
    [[TOLLookAPIHTTPClient v1SharedClient] allCategoriesForType:TOLLookCategoryTypeSupplier withCompletion:^(NSArray *lookCategories, NSError *error){
        
        if (error) {
            return;
        }
        
        NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
        
        TOLLookCategoryType suppliersType = TOLLookCategoryTypeSupplier;
        
        [lookCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSDictionary *parameters = @{@"idOnServer" : obj[@"id"],
                                         @"title" : obj[@"title"],
                                         @"thumbURL" : obj[@"thumb"],
                                         @"entries" : obj[@"entries"],
                                         @"type" : @(suppliersType)};
            
            [LookCategory createOrUpdateLookCategoryWithParameters:parameters inContext:mainContext];
        }];
        
        [mainContext saveAndWait];
        
    }];

    
    //TODO: Remove that from here!!!!!
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is2lookInstalled"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setupUser
{
    NSManagedObjectContext *mainContext = [NSManagedObjectContext mainContext];
    
    if ([User currentUserInContext:mainContext]) {
        
        return;
    }
    
    [[TOLLookAPIHTTPClient  v1SharedClient] createUserWithCompletion:^(NSString *idOnAPICreated, NSError *error){
        
        User *user = [User createUserWithIdOnAPI:idOnAPICreated inContext:mainContext];
        
        [User setCurrentUser:user inContext:mainContext];
        
    }];
    
}

@end
