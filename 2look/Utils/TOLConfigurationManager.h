//
//  TOLConfigurationManager.h
//  2look
//
//  Created by Marcio Klepacz on 5/30/14.
//  Copyright (c) 2014 Marcio Klepacz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TOLConfigurationManager : NSObject

+ (instancetype)sharedInstance;

- (void)setupCoreData;

- (void)setupUser;

@end
